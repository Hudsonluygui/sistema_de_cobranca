package Interfaces;

import Classes.ClasseCliente;
import Classes.ControleCliente;
import Validações.ConverterDecimais;
import Validações.LimparCampos;
import Validações.LimparTabelas;
import Validações.Preencher_JTableGenerico;
import java.awt.Color;
import java.awt.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

public class Consultar extends javax.swing.JFrame {
    
    ConverterDecimais Converter;
    LimparTabelas Limpar_Tabela;
    Preencher_JTableGenerico preencher;
    ClasseCliente Classe;
    ControleCliente Controle;
    LimparCampos Limpar_Campos;
    public String Data;
    public String Vencimento;
    public int Meses;
    
    public Consultar() {
        initComponents();
        Classe = new ClasseCliente();
        Controle = new ControleCliente();
        Limpar_Campos = new LimparCampos();
        Limpar_Tabela = new LimparTabelas();
        preencher = new Preencher_JTableGenerico();
        
        preencher.Preencher_JTableGenerico(JTPesquisa, Controle.PesquisarVencido());
        
        Tamanho_Jtable(JTPesquisa, 0, 250);
        Tamanho_Jtable(JTPesquisa, 1, 100);
        Tamanho_Jtable(JTPesquisa, 2, 150);
        Tamanho_Jtable(JTPesquisa, 3, 150);
        Tamanho_Jtable(JTPesquisa, 4, 100);
        Tamanho_Jtable(JTPesquisa, 5, 100);
        Tamanho_Jtable(JTPesquisa, 6, 50);
        Data = Controle.DATA_ATUAL();
        int linhas = JTPesquisa.getRowCount();
        JTFPago.setEditable(false);
        JBLiquidar.setEnabled(false);
        /*
         for (int X = 0; X < linhas; X++) {
         Vencimento = (String) JTPesquisa.getValueAt(X, 2);
         CalcularMeses();
         if (Meses >= 6) {
         JTPesquisa.setBackground(Color.red);
              

         }
         }
         for (int X = 0; X < linhas; X++) {
         Vencimento = (String) JTPesquisa.getValueAt(X, 2);
         CalcularMeses();
         if (Meses < 6) {
                
         JTPesquisa.setBackground(Color.YELLOW);
              

         }
         }
         */
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        JTPesquisa = new javax.swing.JTable();
        JBLiquidar = new javax.swing.JButton();
        JTFPago = new javax.swing.JFormattedTextField();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Consultar");
        setResizable(false);

        JTPesquisa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Cliente", "Valor", "Data Vencimento", "Data Cadastro", "Tel.1 ", "Tel. 2", "Codigo"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        JTPesquisa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JTPesquisaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(JTPesquisa);

        JBLiquidar.setText("Liquidar");
        JBLiquidar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBLiquidarActionPerformed(evt);
            }
        });

        try {
            JTFPago.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel1.setText("Data Paga");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 761, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(JTFPago, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(JBLiquidar)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JBLiquidar)
                    .addComponent(JTFPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 467, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void JBLiquidarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBLiquidarActionPerformed
        
        Classe.setSITUACAO("LIQUIDADA");
        Classe.setDATA_PAGAMENTO(JTFPago.getText());
        int opcao = JOptionPane.showConfirmDialog(null, "Confirmar?");
        if (opcao == JOptionPane.YES_OPTION) {
            Controle.Liquidar(Classe);
        }
        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
        preencher.Preencher_JTableGenerico(JTPesquisa, Controle.PesquisarVencido());
        
        JBLiquidar.setEnabled(false);
        JTFPago.setEditable(false);
        JTFPago.setText(null);

    }//GEN-LAST:event_JBLiquidarActionPerformed

    private void JTPesquisaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JTPesquisaMouseClicked
        
        int index = JTPesquisa.getSelectedRow();
        String Codigo = (String) JTPesquisa.getValueAt(index, 6);
        Classe.setID_CLIENTE(Integer.parseInt(Codigo));
        JBLiquidar.setEnabled(true);
        JTFPago.setEditable(true);
// TODO add your handling code here:
    }//GEN-LAST:event_JTPesquisaMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Consultar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Consultar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Consultar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Consultar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Consultar().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton JBLiquidar;
    private javax.swing.JFormattedTextField JTFPago;
    private javax.swing.JTable JTPesquisa;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables

    public void Tamanho_Jtable(JTable tabela, int Coluna, int Tamanho_Desejado) {
        tabela.getColumnModel().getColumn(Coluna).setPreferredWidth(Tamanho_Desejado);
    }
    
    public void CalcularMeses() {
        try {
            // constrói a primeira data
            DateFormat fm = new SimpleDateFormat("dd/MM/yyyy");
//data Vencida
            Date data1 = (Date) fm.parse(Vencimento);

            // constrói a segunda data
            fm = new SimpleDateFormat("dd/MM/yyyy");
            //data atual
            Date data2 = (Date) fm.parse(Data);

            // vamos obter a diferença de meses
            long segundos = (data2.getTime()
                    - data1.getTime()) / 1000;
            Meses = (int) Math.floor(segundos / 2592000);
            segundos -= Meses * 2592000;
            int dias = (int) Math.floor(segundos / 86400);

            // exibe o resultado
            System.out.println("As duas datas tem "
                    + Meses + " meses e " + dias
                    + " dias de diferença");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    
    public class ColorRenderer extends DefaultTableCellRenderer {
        
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            
            if (row % 2 == 0) {
                comp.setBackground(Color.LIGHT_GRAY);
            } else {
                comp.setBackground(Color.WHITE);
            }
            
            return comp;
        }
    }
    
}
