package Interfaces;

import Classes.ClasseUsuario;
import Classes.ControleUsuario;

import java.awt.Color;
import javax.swing.JOptionPane;

public class CadastroUsuario extends javax.swing.JFrame {

    public boolean teste;
    public boolean testeSenha;
    ClasseUsuario login = new ClasseUsuario();
    ControleUsuario Controle = new ControleUsuario();

    public CadastroUsuario() {
        initComponents();
        JLogin.setText(" ");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        JTFLogin = new javax.swing.JTextField();
        JLogin = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        JTFSenha = new javax.swing.JPasswordField();
        jLabel4 = new javax.swing.JLabel();
        JTFSenha2 = new javax.swing.JPasswordField();
        JPAINELBOTAO = new javax.swing.JPanel();
        jBGravar = new javax.swing.JButton();
        jBCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastrar Usuário");
        setResizable(false);

        jLabel3.setText("Login");

        JTFLogin.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFLoginFocusLost(evt);
            }
        });

        JLogin.setText("jLabel1");

        jLabel2.setText("Senha");

        jLabel4.setText("Digite a Senha Novamente");

        JPAINELBOTAO.setBackground(new java.awt.Color(153, 153, 153));

        jBGravar.setText("Confirmar");
        jBGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGravarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBGravar);

        jBCancelar.setText("Cancelar");
        jBCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCancelarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBCancelar);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(JTFLogin)
                    .addComponent(JTFSenha)
                    .addComponent(JTFSenha2)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4)
                            .addComponent(JLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 167, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JTFLogin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JLogin)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JTFSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JTFSenha2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGravarActionPerformed

        if (JTFLogin.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Informe um Nome de Usuário Válido");
            return;
        } else if (JTFSenha.getText().equals("") || (JTFSenha.getText().equals(" "))) {
            JOptionPane.showMessageDialog(null, "Informe a Senha");
            return;
        } else if (JTFSenha2.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Por Favor Redigite a Senha");
            return;
        }
        if (!JTFSenha.getText().equals(JTFSenha2.getText())) {
            testeSenha = false;
        } else {
            testeSenha = true;
        }

        if (testeSenha == false) {
            JOptionPane.showMessageDialog(null, "Senhas não são Iguais");
            return;
        }
        login.setSENHA(JTFSenha.getText().toUpperCase());
        login.setUSUARIO(JTFLogin.getText().toUpperCase());
        if (teste == false) {
            JOptionPane.showMessageDialog(null, "Usuário não está Disponível");
            return;
        }
        if (Controle.Cadastrar(login)) {
            JOptionPane.showMessageDialog(null, "Usuário Cadastrado");
            JTFSenha.setText(null);
            JTFSenha2.setText(null);
            JTFLogin.setText(null);
            JLogin.setText(" ");

        }
    }//GEN-LAST:event_jBGravarActionPerformed

    private void jBCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_jBCancelarActionPerformed

    private void JTFLoginFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFLoginFocusLost
        if (!JTFLogin.getText().equals("")) {
            login.setUSUARIO(JTFLogin.getText().toUpperCase());
            if (Controle.BuscarUsuario(login)) {
                JLogin.setText("Usuário já Cadastrado");
                JLogin.setForeground(Color.red);
                teste = false;
                return;
            } else {
                teste = true;
                JLogin.setText("Usuário OK");
                JLogin.setForeground(Color.blue);
            }
        }

// TODO add your handling code here:
    }//GEN-LAST:event_JTFLoginFocusLost

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CadastroUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CadastroUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CadastroUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CadastroUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CadastroUsuario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel JLogin;
    private javax.swing.JPanel JPAINELBOTAO;
    private javax.swing.JTextField JTFLogin;
    private javax.swing.JPasswordField JTFSenha;
    private javax.swing.JPasswordField JTFSenha2;
    private javax.swing.JButton jBCancelar;
    private javax.swing.JButton jBGravar;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
