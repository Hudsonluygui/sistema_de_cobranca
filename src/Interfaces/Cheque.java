package Interfaces;

import Classes.ClasseCheque;
import Classes.ControleCheque;
import Validações.LimparCampos;
import Validações.LimparTabelas;
import Validações.Preencher_JTableGenerico;
import java.awt.event.MouseEvent;
import javax.swing.JOptionPane;
import javax.swing.JTable;

/**
 *
 * @author Hudson
 */
public class Cheque extends javax.swing.JFrame {

    ControleCheque Controle;
    ClasseCheque Classe;
    LimparCampos Limpar_Campos;
    LimparTabelas Limpar_Tabela;
    Preencher_JTableGenerico preencher;
    int Codigo = 0;

    public Cheque() {
        initComponents();
        Controle = new ControleCheque();
        Classe = new ClasseCheque();
        Limpar_Campos = new LimparCampos();
        Limpar_Tabela = new LimparTabelas();
        preencher = new Preencher_JTableGenerico();
        JBExcluir.setEnabled(false);

        Tamanho_Jtable(JTPesquisa, 0, 230);
        Tamanho_Jtable(JTPesquisa, 1, 230);
        Tamanho_Jtable(JTPesquisa, 2, 200);
        Tamanho_Jtable(JTPesquisa, 3, 100);
        Tamanho_Jtable(JTPesquisa, 4, 150);
        Tamanho_Jtable(JTPesquisa, 5, 150);
        Tamanho_Jtable(JTPesquisa, 6, 150);
        Tamanho_Jtable(JTPesquisa, 7, 50);
        Tamanho_Jtable(JTPesquisa, 8, 200);
        Tamanho_Jtable(JTPesquisa, 9, 100);
        Tamanho_Jtable(JTPesquisa, 10, 150);
        Tamanho_Jtable(JTPesquisa, 11, 50);

        Double_Click();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JTFTitular = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        JTFCliente = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        JTFCidade = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        JTFValor = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        JTFDataEmissao = new javax.swing.JFormattedTextField();
        jLabel6 = new javax.swing.JLabel();
        JTFDataVenc = new javax.swing.JFormattedTextField();
        jLabel7 = new javax.swing.JLabel();
        JCPago = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        JTFNomeRecebido = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        JTFValorPago = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        JTFTelefone = new javax.swing.JFormattedTextField();
        jLabel10 = new javax.swing.JLabel();
        JTFDataRecebido = new javax.swing.JFormattedTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        JCPesquisa = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        JTPesquisa = new javax.swing.JTable();
        JTFPesquisar = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        JPAINELBOTAO = new javax.swing.JPanel();
        jBGravar = new javax.swing.JButton();
        JBExcluir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastrar Cheques");
        setResizable(false);

        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });

        jLabel1.setText("Titular do Cheque");

        jLabel2.setText("Cliente que Repassou o Cheque");

        jLabel3.setText("Cidade");

        jLabel4.setText("Valor");

        jLabel5.setText("Data de Emissao");

        try {
            JTFDataEmissao.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel6.setText("Data de Vencimento");

        try {
            JTFDataVenc.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel7.setText("Foi Pago?");

        JCPago.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "NÃO!", "SIM!" }));
        JCPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCPagoActionPerformed(evt);
            }
        });

        jLabel8.setBackground(new java.awt.Color(0, 0, 0));
        jLabel8.setForeground(new java.awt.Color(255, 0, 0));
        jLabel8.setText("*Se sim, Recebido por Quem?");

        jLabel9.setForeground(new java.awt.Color(255, 0, 0));
        jLabel9.setText("*Valor Recebido");

        jLabel11.setText("Telefone Para Contato");

        try {
            JTFTelefone.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##)####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel10.setForeground(new java.awt.Color(255, 0, 0));
        jLabel10.setText("*Data do Recebimento");

        try {
            JTFDataRecebido.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JTFTitular)
                    .addComponent(JTFCliente)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(JTFTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(JTFCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(JTFValor)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(JTFValorPago, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(JTFDataRecebido))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(JTFDataEmissao, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel9))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel10)
                                    .addComponent(jLabel6)
                                    .addComponent(JTFDataVenc, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(7, 7, 7)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(JCPago, 0, 82, Short.MAX_VALUE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(0, 306, Short.MAX_VALUE))
                            .addComponent(JTFNomeRecebido))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JTFTitular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JTFCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel11)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(JTFDataEmissao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(JTFDataVenc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(JCPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(JTFNomeRecebido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(jLabel8)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFValorPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFDataRecebido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(263, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Cadastro", jPanel1);

        jLabel12.setForeground(new java.awt.Color(255, 0, 0));
        jLabel12.setText("*Para fazer Alterações Clique 2x");

        JCPesquisa.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Geral", "Titular", "Cliente", "Cidade" }));
        JCPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCPesquisaActionPerformed(evt);
            }
        });

        JTPesquisa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Titular", "Cliente", "Cidade", "Valor", "Telefone", "Data Emissao", "Data Vencimento", "Pago", "Nome Entregue", "Valor Recebido", "Data Recebida", "Codigo"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        JTPesquisa.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        JTPesquisa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JTPesquisaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(JTPesquisa);

        JTFPesquisar.setEditable(false);

        jButton1.setText("Pesquisar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 899, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JTFPesquisar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel12)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 431, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Consulta", jPanel2);

        JPAINELBOTAO.setBackground(new java.awt.Color(153, 153, 153));

        jBGravar.setText("Gravar");
        jBGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGravarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBGravar);

        JBExcluir.setText("Excluir");
        JBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBExcluirActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(JBExcluir);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1)
                    .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void JCPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCPagoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JCPagoActionPerformed

    private void jBGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGravarActionPerformed
        if (Classe.getID_CHEQUE() == 0) {
            if (JCPago.getSelectedIndex() == 0) {
                if (Controle.CadastrarSemData(GetCampos())) {
                    JOptionPane.showMessageDialog(null, "Cheque Cadastrado com Sucesso");
                    Limpar_Campos.LimparCampos(jPanel1);
                } else {
                    JOptionPane.showMessageDialog(null, "Ocorreu Algum Erro: " + Classe.getERRO());
                }
            } else if (JCPago.getSelectedIndex() == 1) {
                if (Controle.CadastrarComData(GetCampos())) {
                    JOptionPane.showMessageDialog(null, "Cheque Cadastrado com Sucesso");
                    Limpar_Campos.LimparCampos(jPanel1);
                } else {
                    JOptionPane.showMessageDialog(null, "Ocorreu Algum Erro: " + Classe.getERRO());
                }
            }

        } else if (Controle.Alterar(GetCampos())) {
            JOptionPane.showMessageDialog(null, "Registro Alterado");
            Limpar_Campos.LimparCampos(jPanel1);
        } else {
            JOptionPane.showMessageDialog(null, "Ocorreu Algum Erro: " + Classe.getERRO());
        }


    }//GEN-LAST:event_jBGravarActionPerformed

    private void JBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBExcluirActionPerformed
        int opcao = JOptionPane.showConfirmDialog(null, "Confirmar Exclusão?");
        if (opcao == JOptionPane.YES_OPTION) {
            Classe.setID_CHEQUE(Codigo);
            Controle.Excluir(Classe);
            Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
            preencher.Preencher_JTableGenerico(JTPesquisa, Controle.PesquisarGeral());
        }
        JBExcluir.setEnabled(false);
        Limpar_Campos.LimparCampos(jPanel1);

    }//GEN-LAST:event_JBExcluirActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        switch (JCPesquisa.getSelectedIndex()) {
            case 0: {
                preencher.Preencher_JTableGenerico(JTPesquisa, Controle.PesquisarGeral());
                break;
            }
            case 1: {
                preencher.Preencher_JTableGenerico(JTPesquisa, Controle.PesquisarTitular(JTFPesquisar.getText().toUpperCase()));
                break;
            }
            case 2: {
                preencher.Preencher_JTableGenerico(JTPesquisa, Controle.PesquisarCliente(JTFPesquisar.getText().toUpperCase()));
                break;
            }
            case 3: {
                preencher.Preencher_JTableGenerico(JTPesquisa, Controle.PesquisarCidade(JTFPesquisar.getText().toUpperCase()));
                break;
            }
        }

    }//GEN-LAST:event_jButton1ActionPerformed

    private void JCPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCPesquisaActionPerformed

        if (JCPesquisa.getSelectedIndex() != 0) {
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
        } else {
            JTFPesquisar.setEditable(false);
            JTFPesquisar.setText("");
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_JCPesquisaActionPerformed

    private void JTPesquisaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JTPesquisaMouseClicked
        int index = JTPesquisa.getSelectedRow();
        String A = (String) JTPesquisa.getValueAt(index, 11);
        Codigo = Integer.parseInt(A);
        JBExcluir.setEnabled(true);

        String Titular = (String) JTPesquisa.getValueAt(index, 0);
        String Cliente = (String) JTPesquisa.getValueAt(index, 1);
        String Cidade = (String) JTPesquisa.getValueAt(index, 2);
        String Valor = (String) JTPesquisa.getValueAt(index, 3);
        String Telefone = (String) JTPesquisa.getValueAt(index, 4);
        String Data_Emissao = (String) JTPesquisa.getValueAt(index, 5);
        String Data_Vencimento = (String) JTPesquisa.getValueAt(index, 6);
        String Pago = (String) JTPesquisa.getValueAt(index, 7);
        String Nome = (String) JTPesquisa.getValueAt(index, 8);
        String Valor_Recebido = (String) JTPesquisa.getValueAt(index, 9);
        String Data_Recebido = (String) JTPesquisa.getValueAt(index, 10);

        Classe.setID_CHEQUE(Codigo);
        Classe.setCIDADE(Cidade);
        Classe.setCLIENTE(Cliente);
        Classe.setDATA_EMISSAO(Data_Emissao);
        Classe.setDATA_RECEBIDO(Data_Recebido);
        Classe.setDATA_VENCIMENTO(Data_Vencimento);
        Classe.setNOME_RECEBIDO(Nome);
        Classe.setPAGO(Pago);
        Classe.setTELEFONE(Telefone);
        Classe.setTITULAR(Titular);
        Classe.setVALOR(Double.parseDouble(Valor));
        try {
            Classe.setVALOR_RECEBIDO(Double.parseDouble(Valor_Recebido));
        } catch (NullPointerException ex) {
            Classe.setVALOR_RECEBIDO(0.00);
        }


    }//GEN-LAST:event_JTPesquisaMouseClicked

    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged

        if (jPanel1.isShowing()) {
            Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
            JTFPesquisar.setText("");
            JCPesquisa.setSelectedIndex(0x0);

        }
        // TODO add your handling code here:
    }//GEN-LAST:event_jTabbedPane1StateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Cheque.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Cheque.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Cheque.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Cheque.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Cheque().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton JBExcluir;
    private javax.swing.JComboBox<String> JCPago;
    private javax.swing.JComboBox<String> JCPesquisa;
    private javax.swing.JPanel JPAINELBOTAO;
    private javax.swing.JTextField JTFCidade;
    private javax.swing.JTextField JTFCliente;
    private javax.swing.JFormattedTextField JTFDataEmissao;
    private javax.swing.JFormattedTextField JTFDataRecebido;
    private javax.swing.JFormattedTextField JTFDataVenc;
    private javax.swing.JTextField JTFNomeRecebido;
    private javax.swing.JTextField JTFPesquisar;
    private javax.swing.JFormattedTextField JTFTelefone;
    private javax.swing.JTextField JTFTitular;
    private javax.swing.JTextField JTFValor;
    private javax.swing.JTextField JTFValorPago;
    private javax.swing.JTable JTPesquisa;
    private javax.swing.JButton jBGravar;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    // End of variables declaration//GEN-END:variables

    public ClasseCheque GetCampos() {
        Classe.setCIDADE(JTFCidade.getText().toUpperCase());
        Classe.setCLIENTE(JTFCliente.getText().toUpperCase());
        Classe.setDATA_EMISSAO(JTFDataEmissao.getText());
        Classe.setDATA_VENCIMENTO(JTFDataVenc.getText());
        Classe.setERRO(null);

        switch (JCPago.getSelectedIndex()) {
            case 0: {
                Classe.setPAGO("NÃO");
                Classe.setDATA_RECEBIDO(null);
                 Classe.setNOME_RECEBIDO(JTFNomeRecebido.getText());
                break;
            }
            case 1: {
                Classe.setPAGO("SIM");
                Classe.setDATA_RECEBIDO(JTFDataRecebido.getText());
                Classe.setNOME_RECEBIDO(JTFNomeRecebido.getText());
                break;
            }
        }
        Classe.setTELEFONE(JTFTelefone.getText());
        Classe.setTITULAR(JTFTitular.getText().toUpperCase());
        if (!JTFValor.getText().equals("")) {
            Classe.setVALOR(Double.parseDouble(JTFValor.getText().replaceAll(",", ".")));
        }
        if (!JTFValorPago.getText().equals("")) {
            Classe.setVALOR_RECEBIDO(Double.parseDouble(JTFValorPago.getText().replaceAll(",", ".")));
        }
        return Classe;
    }

    public void Tamanho_Jtable(JTable tabela, int Coluna, int Tamanho_Desejado) {
        tabela.getColumnModel().getColumn(Coluna).setPreferredWidth(Tamanho_Desejado);
    }

    public void Double_Click() {
        JTPesquisa.addMouseListener(
                new java.awt.event.MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                // Se o botão direito do mouse foi pressionado  
                if (e.getClickCount() == 2) {
                    //Se for 2x
                    retornar();
                    JBExcluir.setEnabled(false);
                }
            }
        });
    }

    public void retornar() {
        JTFCidade.setText(Classe.getCIDADE());
        JTFCliente.setText(Classe.getCLIENTE());
        JTFDataEmissao.setText(Classe.getDATA_EMISSAO());
        JTFDataRecebido.setText(Classe.getDATA_RECEBIDO());
        JTFDataVenc.setText(Classe.getDATA_VENCIMENTO());
        JTFNomeRecebido.setText(Classe.getNOME_RECEBIDO());
        JTFTelefone.setText(Classe.getTELEFONE());
        JTFTitular.setText(Classe.getTITULAR());
        JTFValor.setText(String.valueOf(Classe.getVALOR()));
        JTFValorPago.setText(String.valueOf(Classe.getVALOR_RECEBIDO()));
        if (Classe.getPAGO().equals("SIM")) {
            JCPago.setSelectedIndex(1);
        } else {
            JCPago.setSelectedIndex(0);
        }
        jTabbedPane1.setSelectedIndex(0);
    }
}
