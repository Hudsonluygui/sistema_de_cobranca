package Interfaces;

import Classes.ClasseCliente;
import Classes.ControleCliente;
import Validações.ConverterDecimais;
import Validações.LimparCampos;
import Validações.LimparTabelas;
import Validações.Preencher_JTableGenerico;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Clientes extends javax.swing.JFrame {

    ConverterDecimais Converter;
    LimparTabelas Limpar_Tabela;
    Preencher_JTableGenerico preencher;
    ClasseCliente Classe;
    ControleCliente Controle;
    LimparCampos Limpar_Campos;

    public Clientes() {
        initComponents();
        Classe = new ClasseCliente();
        Controle = new ControleCliente();
        JBExcluir.setEnabled(false);
        Limpar_Campos = new LimparCampos();
        Limpar_Tabela = new LimparTabelas();
        preencher = new Preencher_JTableGenerico();

        JTFDataAtual.setText(Controle.DATA_ATUAL());

        Tamanho_Jtable(JTPEsquisa, 0, 250);
        Tamanho_Jtable(JTPEsquisa, 1, 100);
        Tamanho_Jtable(JTPEsquisa, 2, 100);
        Tamanho_Jtable(JTPEsquisa, 3, 100);
        Tamanho_Jtable(JTPEsquisa, 4, 100);
        Tamanho_Jtable(JTPEsquisa, 5, 100);
        Tamanho_Jtable(JTPEsquisa, 6, 50);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JTFNome = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        JTFCidade = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        JTFTel1 = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        JTFTel2 = new javax.swing.JFormattedTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        JTFValor = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        JTFDataVenc = new javax.swing.JFormattedTextField();
        jLabel7 = new javax.swing.JLabel();
        JTFDataAtual = new javax.swing.JFormattedTextField();
        JCSituacao = new javax.swing.JComboBox();
        jLabel8 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jButton3 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        JTPEsquisa = new javax.swing.JTable();
        JCPesquisa = new javax.swing.JComboBox();
        JTFPesquisa = new javax.swing.JTextField();
        JPAINELBOTAO = new javax.swing.JPanel();
        jBGravar = new javax.swing.JButton();
        JBExcluir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Clientes");
        setResizable(false);

        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });

        jLabel1.setText("Nome");

        jLabel2.setText("Cidade");

        jLabel3.setText("Telefone 1");

        try {
            JTFTel1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##)####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel4.setText("Telefone 2");

        try {
            JTFTel2.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##)####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel5.setText("Valor");

        jLabel6.setText("Data Vencimento");

        try {
            JTFDataVenc.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel7.setText("Data Atual");

        JTFDataAtual.setEditable(false);
        try {
            JTFDataAtual.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        JCSituacao.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ABERTA", "LIQUIDADA" }));

        jLabel8.setText("Situação");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(JTFTel2, javax.swing.GroupLayout.DEFAULT_SIZE, 241, Short.MAX_VALUE)
                            .addComponent(JTFNome)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(JTFCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(JTFTel1))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addGap(163, 163, 163)
                                        .addComponent(jLabel3))
                                    .addComponent(JCSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addComponent(jSeparator1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(200, 200, 200)
                                .addComponent(jLabel8))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(230, 230, 230)
                                .addComponent(jLabel6))
                            .addComponent(jLabel7)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(JTFDataAtual, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(JTFValor, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(JTFDataVenc, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 232, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFTel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFTel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JCSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFDataVenc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JTFDataAtual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(117, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Dados", jPanel1);

        jButton3.setText("Pesquisar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        JTPEsquisa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Cliente", "Valor", "Data Vencimento", "Data Cadastro", "Tel. 1", "Tel. 2", "Código"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        JTPEsquisa.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        JTPEsquisa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JTPEsquisaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(JTPEsquisa);

        JCPesquisa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Geral", "Nome" }));
        JCPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCPesquisaActionPerformed(evt);
            }
        });

        JTFPesquisa.setEditable(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 672, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(JTFPesquisa))
                        .addGap(18, 18, 18)
                        .addComponent(jButton3)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Consulta", jPanel2);

        JPAINELBOTAO.setBackground(new java.awt.Color(153, 153, 153));

        jBGravar.setText("Gravar");
        jBGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGravarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBGravar);

        JBExcluir.setText("Excluir");
        JBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBExcluirActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(JBExcluir);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1)
                    .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 367, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGravarActionPerformed
        if (JTFNome.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Informe o Nome do Cliente");
            JTFNome.requestFocus();
        } else if (JTFValor.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Informe o Valor");
            JTFValor.requestFocus();
        } else if (JTFDataVenc.getText().equals("  /  /    ")) {
            JOptionPane.showMessageDialog(null, "Informe a Data que Venceu");
            JTFDataVenc.requestFocus();
        } else {
            GetCampos();
            if (Controle.Cadastrar(Classe)) {
                JOptionPane.showMessageDialog(null, "Cliente Cadastrado");
                Limpar_Campos.LimparCampos(jPanel1);
            }
            JTFDataAtual.setEditable(false);
            JTFDataAtual.setText(Controle.DATA_ATUAL());

        }
    }//GEN-LAST:event_jBGravarActionPerformed

    private void JBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBExcluirActionPerformed

        int opcao = JOptionPane.showConfirmDialog(null, "Confirmar Exclusão?");
        if (opcao == JOptionPane.YES_OPTION) {
            Controle.Excluir(Classe);
        }
        Limpar_Tabela.Limpar_tabela_pesquisa(JTPEsquisa);
        preencher.Preencher_JTableGenerico(JTPEsquisa, Controle.Pesquisar());

        JBExcluir.setEnabled(false);
    }//GEN-LAST:event_JBExcluirActionPerformed

    private void JCPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCPesquisaActionPerformed

        if (JCPesquisa.getSelectedIndex() == 1) {
            JTFPesquisa.setText("");
            JTFPesquisa.setEditable(true);
            JTFPesquisa.requestFocus();

        } else if (JCPesquisa.getSelectedIndex() == 0) {
            JTFPesquisa.setText("");

            JTFPesquisa.setEditable(false);
        }
// TODO add your handling code here:
    }//GEN-LAST:event_JCPesquisaActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

        switch (JCPesquisa.getSelectedIndex()) {
            case 0: {
                preencher.Preencher_JTableGenerico(JTPEsquisa, Controle.Pesquisar());
                break;
            }
            case 1: {
                preencher.Preencher_JTableGenerico(JTPEsquisa, Controle.Pesquisar(JTFPesquisa.getText().toUpperCase()));
                break;
            }
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void JTPEsquisaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JTPEsquisaMouseClicked

        int index = JTPEsquisa.getSelectedRow();
        JBExcluir.setEnabled(true);
        String Codigo = (String) JTPEsquisa.getValueAt(index, 6);
        Classe.setID_CLIENTE(Integer.parseInt(Codigo));


    }//GEN-LAST:event_JTPEsquisaMouseClicked

    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged

        if (jPanel1.isShowing()) {
            JBExcluir.setEnabled(false);
            Limpar_Tabela.Limpar_tabela_pesquisa(JTPEsquisa);
        } else if (jPanel2.isShowing()) {
            JBExcluir.setEnabled(false);
            Limpar_Tabela.Limpar_tabela_pesquisa(JTPEsquisa);
            JCPesquisa.setSelectedIndex(0x0);
        }
// TODO add your handling code here:
    }//GEN-LAST:event_jTabbedPane1StateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Clientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Clientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Clientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Clientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Clientes().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton JBExcluir;
    private javax.swing.JComboBox JCPesquisa;
    private javax.swing.JComboBox JCSituacao;
    private javax.swing.JPanel JPAINELBOTAO;
    private javax.swing.JTextField JTFCidade;
    private javax.swing.JFormattedTextField JTFDataAtual;
    private javax.swing.JFormattedTextField JTFDataVenc;
    private javax.swing.JTextField JTFNome;
    private javax.swing.JTextField JTFPesquisa;
    private javax.swing.JFormattedTextField JTFTel1;
    private javax.swing.JFormattedTextField JTFTel2;
    private javax.swing.JTextField JTFValor;
    private javax.swing.JTable JTPEsquisa;
    private javax.swing.JButton jBGravar;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane1;
    // End of variables declaration//GEN-END:variables

    public ControleCliente GetCampos() {
        Classe.setCIDADE(JTFCidade.getText().toUpperCase());
        Classe.setDATA_ATUAL(JTFDataAtual.getText());
        Classe.setDATA_VENCIMENTO(JTFDataVenc.getText());
        Classe.setNOME(JTFNome.getText().toUpperCase());
        Classe.setTEL1(JTFTel1.getText());
        Classe.setTEL2(JTFTel2.getText());
        Classe.setVALOR(Double.parseDouble(JTFValor.getText()));
        switch (JCSituacao.getSelectedIndex()){
            case 0: {
              Classe.setSITUACAO("ABERTA");
              break;
            }case 1: {
            Classe.setSITUACAO("LIQUIDADA");
            break;
            }
        }
        return Controle;
    }

    public void Tamanho_Jtable(JTable tabela, int Coluna, int Tamanho_Desejado) {
        tabela.getColumnModel().getColumn(Coluna).setPreferredWidth(Tamanho_Desejado);
    }

}
