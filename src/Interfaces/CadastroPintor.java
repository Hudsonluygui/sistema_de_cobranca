package Interfaces;

import Classes.ClassePintor;
import Classes.ControlePintor;
import Validações.LimparCampos;
import Validações.LimparTabelas;
import Validações.Preencher_JTableGenerico;
import javax.swing.JOptionPane;
import javax.swing.JTable;

public class CadastroPintor extends javax.swing.JFrame {

    ControlePintor Controle;
    ClassePintor Classe;
    LimparCampos Limpar_Campos;
    LimparTabelas Limpar_Tabela;
    Preencher_JTableGenerico preencher;

    public CadastroPintor() {
        initComponents();
        Controle = new ControlePintor();
        Classe = new ClassePintor();
        Limpar_Campos = new LimparCampos();
        Limpar_Tabela = new LimparTabelas();
        preencher = new Preencher_JTableGenerico();
        JBExcluir.setEnabled(false);
        JTFNome.requestFocus();

        Tamanho_Jtable(JTPesquisa, 0, 100);
        Tamanho_Jtable(JTPesquisa, 1, 100);
        Tamanho_Jtable(JTPesquisa, 2, 100);
        Tamanho_Jtable(JTPesquisa, 3, 100);
        Tamanho_Jtable(JTPesquisa, 4, 100);
        Tamanho_Jtable(JTPesquisa, 5, 300);
        Tamanho_Jtable(JTPesquisa, 6, 150);
        Tamanho_Jtable(JTPesquisa, 7, 150);
        Tamanho_Jtable(JTPesquisa, 8, 350);
        Tamanho_Jtable(JTPesquisa, 9, 50);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JTFCodigo = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        JTFNome = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        JTFTel = new javax.swing.JFormattedTextField();
        JTFEndereco = new javax.swing.JTextField();
        JTFNum = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        JTFCidade = new javax.swing.JTextField();
        JTFTel2 = new javax.swing.JFormattedTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        JTFData_Nasc = new javax.swing.JFormattedTextField();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        JTFObs = new javax.swing.JTextArea();
        jLabel10 = new javax.swing.JLabel();
        JCGrupo = new javax.swing.JComboBox<>();
        jLabel12 = new javax.swing.JLabel();
        JCVendedor = new javax.swing.JComboBox();
        jPanel2 = new javax.swing.JPanel();
        JCPesquisa = new javax.swing.JComboBox();
        JTFPesquisar = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        JTPesquisa = new javax.swing.JTable();
        jButton3 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jButton2 = new javax.swing.JButton();
        JPAINELBOTAO = new javax.swing.JPanel();
        jBGravar = new javax.swing.JButton();
        JBExcluir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro Pintor");
        setResizable(false);

        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });

        jLabel1.setText("Código");

        JTFCodigo.setEditable(false);

        jLabel2.setText("Nome");

        jLabel3.setText("Endereço");

        jLabel4.setText("Telefone");

        try {
            JTFTel.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##)####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel5.setText("Número");

        jLabel6.setText("Cidade");

        try {
            JTFTel2.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##)####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel7.setText("Telefone 2");

        jLabel8.setText("Data Nascimento");

        try {
            JTFData_Nasc.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel9.setText("Observações");

        JTFObs.setColumns(20);
        JTFObs.setRows(5);
        jScrollPane1.setViewportView(JTFObs);

        jLabel10.setText("Grupo");

        JCGrupo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Grupo 1", "Grupo 2", "Grupo 3", "Grupo 4", "Grupo 5" }));

        jLabel12.setText("Vendedor");

        JCVendedor.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Diego", "Evandro", "Igor", "Joacir", "Lara", "Wagner", "Luiz Carlos" }));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(JTFNome, javax.swing.GroupLayout.DEFAULT_SIZE, 499, Short.MAX_VALUE)
                                .addComponent(JTFEndereco))
                            .addComponent(jLabel2)
                            .addComponent(jLabel1)
                            .addComponent(JTFCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel4))
                                .addGap(144, 144, 144)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel7))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(JTFNum, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(JTFTel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(JTFTel2)
                                    .addComponent(JTFCidade)))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(155, 155, 155)
                                .addComponent(jLabel10)
                                .addGap(85, 85, 85)
                                .addComponent(jLabel12))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(JTFData_Nasc, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(JCGrupo, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(JCVendedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel9)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 499, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 405, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JTFCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFTel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFTel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFNum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel10)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFData_Nasc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JCGrupo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JCVendedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(171, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Cadastro", jPanel1);

        JCPesquisa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Geral", "Código", "Nome", "Cidade" }));
        JCPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCPesquisaActionPerformed(evt);
            }
        });

        JTFPesquisar.setEditable(false);

        jButton1.setText("Pesquisar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        JTPesquisa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Vendedor", "Pintor", "Tel1", "Tel2", "Cidade", "Endereco", "Num", "Data Nascimento", "Observação", "Grupo", "Código"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        JTPesquisa.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        JTPesquisa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JTPesquisaMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(JTPesquisa);

        jButton3.setText("Imprimir");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(JTFPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 728, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton3)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(jButton3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 411, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Consulta", jPanel2);

        jLabel11.setForeground(new java.awt.Color(255, 0, 0));
        jLabel11.setText("Selecione o Grupo e clique em Pesquisar");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Grupo 1", "Grupo 2", "Grupo 3", "Grupo 4", "Grupo 5" }));

        jButton2.setText("Pesquisar Grupo");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2)))
                .addContainerGap(723, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2))
                .addContainerGap(428, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Grupo", jPanel3);

        JPAINELBOTAO.setBackground(new java.awt.Color(153, 153, 153));

        jBGravar.setText("Gravar");
        jBGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGravarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBGravar);

        JBExcluir.setText("Excluir");
        JBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBExcluirActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(JBExcluir);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1)
                    .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 510, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGravarActionPerformed

        if (JTFCodigo.getText().equals("")) {
            if (JTFData_Nasc.getText().equals("  /  /    ")) {
                if (Controle.CadastrarSemData(GetCampos()));
                JTFCodigo.setText(String.valueOf(Classe.getID_PINTOR()));
                JOptionPane.showMessageDialog(null, "Registro Incluso com Sucesso");
                Limpar_Campos.LimparCampos(jPanel1);
                JTFNome.requestFocus();
            } else {
                if (Controle.CadastrarComData(GetCampos()));
                JTFCodigo.setText(String.valueOf(Classe.getID_PINTOR()));
                JOptionPane.showMessageDialog(null, "Registro Incluso com Sucesso");
                Limpar_Campos.LimparCampos(jPanel1);
                JTFNome.requestFocus();
            }
        } else if (JTFData_Nasc.getText().equals("  /  /    ")) {
            Controle.AlterarSemData(GetCampos());
            JOptionPane.showMessageDialog(null, "Registro Alterado com Sucesso");
            Limpar_Campos.LimparCampos(jPanel1);
        } else {
            Controle.AlterarCom(GetCampos());
            JOptionPane.showMessageDialog(null, "Registro Alterado com Sucesso");
            Limpar_Campos.LimparCampos(jPanel1);
        }
        Limpar_Campos.LimparCampos(jPanel1);
        JTFNome.requestFocus();
    }//GEN-LAST:event_jBGravarActionPerformed

    private void JBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBExcluirActionPerformed

        int opcao = JOptionPane.showConfirmDialog(null, "Confirmar Exclusão?");
        if (opcao == JOptionPane.YES_OPTION) {
            Controle.Excluir(Classe);
            Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
            preencher.Preencher_JTableGenerico(JTPesquisa, Controle.Pesquisar());
        }
        JBExcluir.setEnabled(false);
        Limpar_Campos.LimparCampos(jPanel1);
    }//GEN-LAST:event_JBExcluirActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        switch (JCPesquisa.getSelectedIndex()) {
            case 0: {
                preencher.Preencher_JTableGenerico(JTPesquisa, Controle.Pesquisar());
                break;
            }
            case 1: {
                preencher.Preencher_JTableGenerico(JTPesquisa, Controle.Pesquisar(Integer.parseInt(JTFPesquisar.getText())));
                break;
            }
            case 2: {
                preencher.Preencher_JTableGenerico(JTPesquisa, Controle.Pesquisar(JTFPesquisar.getText().toUpperCase()));
                break;
            }
            case 3: {
                preencher.Preencher_JTableGenerico(JTPesquisa, Controle.PesquisarCidade(JTFPesquisar.getText().toUpperCase()));
                break;
            }
        }

// TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void JCPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCPesquisaActionPerformed

        if (JCPesquisa.getSelectedIndex() == 1) {
            JTFPesquisar.setText("");
            JTFPesquisar.requestFocus();
            JTFPesquisar.setEditable(true);
        } else if (JCPesquisa.getSelectedIndex() == 2) {
            JTFPesquisar.setText("");
            JTFPesquisar.requestFocus();
            JTFPesquisar.setEditable(true);
        } else if (JCPesquisa.getSelectedIndex() == 3) {
            JTFPesquisar.setText("");
            JTFPesquisar.requestFocus();
            JTFPesquisar.setEditable(true);
        } else if (JCPesquisa.getSelectedIndex() == 0) {
            JTFPesquisar.setText("");
            JTFPesquisar.requestFocus();
            JTFPesquisar.setEditable(false);
        }
// TODO add your handling code here:
    }//GEN-LAST:event_JCPesquisaActionPerformed

    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged
        if (jPanel1.isShowing()) {
            Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
        }
// TODO add your handling code here:
    }//GEN-LAST:event_jTabbedPane1StateChanged

    private void JTPesquisaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JTPesquisaMouseClicked

        int index = JTPesquisa.getSelectedRow();
        String Vendedor = (String) JTPesquisa.getValueAt(index, 0);
        String Nome = (String) JTPesquisa.getValueAt(index, 1);
        String Tel1 = (String) JTPesquisa.getValueAt(index, 2);
        String Tel2 = (String) JTPesquisa.getValueAt(index, 3);
        String Cidade = (String) JTPesquisa.getValueAt(index, 4);
        String Endereco = (String) JTPesquisa.getValueAt(index, 5);
        String Num = (String) JTPesquisa.getValueAt(index, 6);
        String Data = (String) JTPesquisa.getValueAt(index, 7);
        String Obs = (String) JTPesquisa.getValueAt(index, 8);
        String Grupo = (String) JTPesquisa.getValueAt(index, 9);
        String Codigo = (String) JTPesquisa.getValueAt(index, 10);

        Classe.setID_PINTOR(Integer.parseInt(Codigo));
        JTFCidade.setText(Cidade);
        JTFCodigo.setText(Codigo);
        JTFData_Nasc.setText(Data);
        JTFEndereco.setText(Endereco);
        JTFNome.setText(Nome);
        JTFNum.setText(Num);
        JTFTel.setText(Tel1);
        JTFTel2.setText(Tel2);
        jPanel1.setVisible(true);
        jPanel1.isShowing();

        if (Vendedor.equals("Diego")) {
            JCVendedor.setSelectedIndex(0);
        } else if (Vendedor.equals("Evandro")) {
            JCVendedor.setSelectedIndex(1);
        } else if (Vendedor.equals("Igor")) {
            JCVendedor.setSelectedIndex(2);
        } else if (Vendedor.equals("Joacir")) {
            JCVendedor.setSelectedIndex(3);
        } else if (Vendedor.equals("Lara")) {
            JCVendedor.setSelectedIndex(4);
        } else if (Vendedor.equals("Wagner")) {
            JCVendedor.setSelectedIndex(5);
        } else if (Vendedor.equals("Luiz Carlos")) {
            JCVendedor.setSelectedIndex(6);
        }
        if (Grupo.equals("Grupo 1")) {
            JCGrupo.setSelectedIndex(0);
        } else if (Grupo.equals("Grupo 2")) {
            JCGrupo.setSelectedIndex(1);
        } else if (Grupo.equals("Grupo 3")) {
            JCGrupo.setSelectedIndex(2);
        } else if (Grupo.equals("Grupo 4")) {
            JCGrupo.setSelectedIndex(3);
        } else if (Grupo.equals("Grupo 5")) {
            JCGrupo.setSelectedIndex(4);
        }
        JBExcluir.setEnabled(true);


    }//GEN-LAST:event_JTPesquisaMouseClicked

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if (jComboBox1.getSelectedIndex() == 0) {
            PesquisarGrupo P = null;
            P.Grupo = "Grupo 1";
            P = new PesquisarGrupo();
            P.show();

        } else if (jComboBox1.getSelectedIndex() == 1) {
            PesquisarGrupo P = null;
            P.Grupo = "Grupo 2";
            P = new PesquisarGrupo();
            P.show();
        } else if (jComboBox1.getSelectedIndex() == 2) {
            PesquisarGrupo P = null;
            P.Grupo = "Grupo 3";
            P = new PesquisarGrupo();
            P.show();
        } else if (jComboBox1.getSelectedIndex() == 3) {
            PesquisarGrupo P = null;
            P.Grupo = "Grupo 4";
            P = new PesquisarGrupo();
            P.show();
        } else if (jComboBox1.getSelectedIndex() == 4) {
            PesquisarGrupo P = null;
            P.Grupo = "Grupo 5";
            P = new PesquisarGrupo();
            P.show();
        }

        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

        Controle.Imprimir();
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CadastroPintor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CadastroPintor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CadastroPintor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CadastroPintor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CadastroPintor().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton JBExcluir;
    private javax.swing.JComboBox<String> JCGrupo;
    private javax.swing.JComboBox JCPesquisa;
    private javax.swing.JComboBox JCVendedor;
    private javax.swing.JPanel JPAINELBOTAO;
    private javax.swing.JTextField JTFCidade;
    private javax.swing.JTextField JTFCodigo;
    private javax.swing.JFormattedTextField JTFData_Nasc;
    private javax.swing.JTextField JTFEndereco;
    private javax.swing.JTextField JTFNome;
    private javax.swing.JTextField JTFNum;
    private javax.swing.JTextArea JTFObs;
    private javax.swing.JTextField JTFPesquisar;
    private javax.swing.JFormattedTextField JTFTel;
    private javax.swing.JFormattedTextField JTFTel2;
    private javax.swing.JTable JTPesquisa;
    private javax.swing.JButton jBGravar;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    // End of variables declaration//GEN-END:variables

    public ClassePintor GetCampos() {
        Classe.setCIDADE(JTFCidade.getText().toUpperCase());
        if (JTFData_Nasc.getText().equals("  /  /    ")) {
            Classe.setDATA_NASCIMENTO(null);
        } else {
            Classe.setDATA_NASCIMENTO(JTFData_Nasc.getText());
        }
        Classe.setENDERECO(JTFEndereco.getText().toUpperCase());
        Classe.setNOME(JTFNome.getText().toUpperCase());
        Classe.setOBS(JTFObs.getText().toUpperCase());
        Classe.setTEL1(JTFTel.getText());
        Classe.setTEL2(JTFTel2.getText());
        Classe.setNUMERO(JTFNum.getText());
        switch (JCGrupo.getSelectedIndex()) {
            case 0: {
                Classe.setGRUPO("Grupo 1");
                break;
            }
            case 1: {
                Classe.setGRUPO("Grupo 2");
                break;
            }
            case 2: {
                Classe.setGRUPO("Grupo 3");
                break;
            }
            case 3: {
                Classe.setGRUPO("Grupo 4");
                break;
            }
            case 4: {
                Classe.setGRUPO("Grupo 5");
                break;
            }
        }

        switch (JCVendedor.getSelectedIndex()) {
            case 0: {
                Classe.setVENDEDOR("Diego");
                break;

            }
            case 1: {
                Classe.setVENDEDOR("Evandro");
                break;
            }
            case 2: {
                Classe.setVENDEDOR("Igor");
                break;
            }
            case 3: {
                Classe.setVENDEDOR("Joacir");
                break;
            }
            case 4: {
                Classe.setVENDEDOR("Lara");
                break;
            }
            case 5: {
                Classe.setVENDEDOR("Wagner");
                break;
            }
            case 6: {
                Classe.setVENDEDOR("Luiz Carlos");
                break;
            }
        }
        return Classe;

    }

    public void Tamanho_Jtable(JTable tabela, int Coluna, int Tamanho_Desejado) {
        tabela.getColumnModel().getColumn(Coluna).setPreferredWidth(Tamanho_Desejado);
    }
}
