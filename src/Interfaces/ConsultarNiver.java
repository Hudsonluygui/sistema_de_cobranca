package Interfaces;

import Classes.ControlePintor;
import Validações.Preencher_JTableGenerico;

public class ConsultarNiver extends javax.swing.JFrame {

    public static String Mes;
    ControlePintor Controle;
    Preencher_JTableGenerico Preencher = new Preencher_JTableGenerico();

    public ConsultarNiver() {
        initComponents();
        Controle = new ControlePintor();
        Pesquisar();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Consultar");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Vendedor", "Nome", "Tel1", "Tel2", "Cidade", "Aniversário"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 819, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 623, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ConsultarNiver.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ConsultarNiver.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ConsultarNiver.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ConsultarNiver.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ConsultarNiver().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables

    public void Pesquisar() {
        if (Mes.equals("Janeiro")) {
            Preencher.Preencher_JTableGenerico(jTable1, Controle.Janeiro());
        } else if (Mes.equals("Fevereiro")) {
            Preencher.Preencher_JTableGenerico(jTable1, Controle.Fevereiro());
        } else if (Mes.equals("Março")) {
            Preencher.Preencher_JTableGenerico(jTable1, Controle.Marco());
        } else if (Mes.equals("Abril")) {
            Preencher.Preencher_JTableGenerico(jTable1, Controle.Abril());
        } else if (Mes.equals("Maio")) {
            Preencher.Preencher_JTableGenerico(jTable1, Controle.Maio());
        } else if (Mes.equals("Junho")) {
            Preencher.Preencher_JTableGenerico(jTable1, Controle.Junho());
        } else if (Mes.equals("Julho")) {
            Preencher.Preencher_JTableGenerico(jTable1, Controle.Julho());
        } else if (Mes.equals("Agosto")) {
            Preencher.Preencher_JTableGenerico(jTable1, Controle.Agosto());
        } else if (Mes.equals("Setembro")) {
            Preencher.Preencher_JTableGenerico(jTable1, Controle.Setembro());
        } else if (Mes.equals("Outubro")) {
            Preencher.Preencher_JTableGenerico(jTable1, Controle.Outubro());
        } else if (Mes.equals("Novembro")) {
            Preencher.Preencher_JTableGenerico(jTable1, Controle.Novembro());
        } else if (Mes.equals("Dezembro")) {
            Preencher.Preencher_JTableGenerico(jTable1, Controle.Dezembro());
        }
    }
}
