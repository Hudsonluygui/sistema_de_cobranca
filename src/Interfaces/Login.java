package Interfaces;

import CONEXAO.ConexaoOracle;
import Classes.ClasseUsuario;
import Classes.ControleUsuario;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;

public class Login extends javax.swing.JFrame {

    private ControleUsuario Controle_Usuario;
    private ClasseUsuario Classe_Usuário;
    public int Cont;

    public Login() {
        initComponents();
        Controle_Usuario = new ControleUsuario();
        Classe_Usuário = new ClasseUsuario();
        Cont = 0;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JTFUser = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        JTFSenha = new javax.swing.JPasswordField();
        JBEntrar = new javax.swing.JButton();
        JBCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Login");
        setResizable(false);

        jLabel3.setText("*O sistema não diferencia letras maiúsculas de minúsculas");

        jLabel4.setText("Todas as informações sao gravadas em Maiusculo por padrão");

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("Usuário");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(11, 10, 0, 0);
        jPanel1.add(jLabel1, gridBagConstraints);

        JTFUser.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JTFUserKeyPressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.ipadx = 270;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 10, 0, 10);
        jPanel1.add(JTFUser, gridBagConstraints);

        jLabel2.setText("Senha");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 10, 0, 0);
        jPanel1.add(jLabel2, gridBagConstraints);

        JTFSenha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JTFSenhaKeyPressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.ipadx = 270;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 10, 0, 10);
        jPanel1.add(JTFSenha, gridBagConstraints);

        JBEntrar.setText("ENTRAR");
        JBEntrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBEntrarActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.ipadx = 12;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(11, 11, 23, 0);
        jPanel1.add(JBEntrar, gridBagConstraints);

        JBCancelar.setText("CANCELAR");
        JBCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBCancelarActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(11, 18, 23, 0);
        jPanel1.add(JBCancelar, gridBagConstraints);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void JBEntrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBEntrarActionPerformed
        if (JTFUser.getText().equals("") || JTFUser.getText().equals(" ")) {
            JOptionPane.showMessageDialog(null, "Informe o Usuário");
            JTFUser.requestFocus();
            return;
        } else if ((JTFSenha.getText().equals("")) || (JTFSenha.getText().equals(""))) {
            JOptionPane.showMessageDialog(null, "Informe a Senha");
            JTFSenha.requestFocus();
            return;
        } else {
            Classe_Usuário.setUSUARIO(JTFUser.getText().toUpperCase());
            Classe_Usuário.setSENHA(JTFSenha.getText().toUpperCase());
            if (!Controle_Usuario.RetornaUsuario(Classe_Usuário)) {
                JOptionPane.showMessageDialog(null, Classe_Usuário.getERRO());
                Cont++;
                if (Cont >= 3) {
                    JOptionPane.showMessageDialog(null, "Você Ultrapassou o Maximo de Tentativas");
                    this.dispose();
                }
            } else {
                this.dispose();
                Menu m = new Menu();
                m.show();
            }
        }

    }//GEN-LAST:event_JBEntrarActionPerformed

    private void JBCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBCancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_JBCancelarActionPerformed

    private void JTFSenhaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTFSenhaKeyPressed
        keyPressed(evt);

// TODO add your handling code here:
    }//GEN-LAST:event_JTFSenhaKeyPressed

    private void JTFUserKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTFUserKeyPressed
        keyPressed1(evt);

    }//GEN-LAST:event_JTFUserKeyPressed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton JBCancelar;
    private javax.swing.JButton JBEntrar;
    private javax.swing.JPasswordField JTFSenha;
    private javax.swing.JTextField JTFUser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables

    public void keyPressed(KeyEvent e) {
        if (e.getSource() == JTFSenha) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                JBEntrarActionPerformed(null);
            }
        }
    }

    public void keyPressed1(KeyEvent e) {
        if (e.getSource() == JTFUser) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                JTFSenha.requestFocus();
            }
        }
    }
}
