package Validações;

import CONEXAO.ConexaoOracle;

import java.sql.SQLException;
import javax.swing.JOptionPane;

public class UltimaSequencia {

    ConexaoOracle conecta_oracle;
    private String utl;

    public UltimaSequencia(String campo, String tabela) {

        conecta_oracle = new ConexaoOracle();
        conecta_oracle.conecta(0);

        try {
            conecta_oracle.executeSQL("SELECT COALESCE(MAX(" + campo + "),0)+1 AS ULTIMO FROM " + tabela);
            conecta_oracle.resultSet.first();
            setUtl(conecta_oracle.resultSet.getString("ULTIMO"));

        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null, erro);


        }
    }

    public UltimaSequencia(String campo1, String campo2, String tabela, String valor) {
        conecta_oracle = new ConexaoOracle();
        conecta_oracle.conecta(0);

        try {
            conecta_oracle.executeSQL("SELECT COALESCE(MAX(" + campo1 + "),0+1 as ultimo from " + tabela + "WHERE "
                    + campo2 + "=" + valor);
            conecta_oracle.resultSet.first();
            setUtl(conecta_oracle.resultSet.getString("ULTIMO"));
        } catch (SQLException erro) {

            JOptionPane.showMessageDialog(null, erro);

        }
    }

    public UltimaSequencia(String campo1, String campo2, String campo3, String tabela, String valor2, String valor3) {
        conecta_oracle = new ConexaoOracle();
        conecta_oracle.conecta(0);

        try {
            conecta_oracle.executeSQL("SELECT COALESCE(MAX(" + campo1 + "),0+1 as ultimo from " + tabela + "WHERE "
                    + campo2 + "=" + valor2 + "AND " + campo3 + "=" + valor3);

            conecta_oracle.resultSet.first();
            setUtl(conecta_oracle.resultSet.getString("ULTIMO"));
            
        } catch (SQLException erro) {

            JOptionPane.showMessageDialog(null, erro);

        }
    }

    public static void main(String[] args) {
        UltimaSequencia us = new UltimaSequencia("CD_MAQUINA", "CAD_MAQUINA");
        System.out.print(us.getUtl());

    }

    /**
     * @return the utl
     */
    public String getUtl() {
        return utl;
    }

    /**
     * @param utl the utl to set
     */
    public void setUtl(String utl) {
        this.utl = utl;
    }
}
