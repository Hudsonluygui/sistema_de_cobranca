package Classes;

public class ClassePintor {

    private int ID_PINTOR;
    private String NOME;
    private String TEL1;
    private String TEL2;
    private String ENDERECO;
    private String NUMERO;
    private String DATA_NASCIMENTO;
    private String OBS;
    private String CIDADE;
    private String ERRO;
private String GRUPO;
private String VENDEDOR;
    /**
     * @return the ID_PINTOR
     */
    public int getID_PINTOR() {
        return ID_PINTOR;
    }

    /**
     * @param ID_PINTOR the ID_PINTOR to set
     */
    public void setID_PINTOR(int ID_PINTOR) {
        this.ID_PINTOR = ID_PINTOR;
    }

    /**
     * @return the NOME
     */
    public String getNOME() {
        return NOME;
    }

    /**
     * @param NOME the NOME to set
     */
    public void setNOME(String NOME) {
        this.NOME = NOME;
    }

    /**
     * @return the TEL1
     */
    public String getTEL1() {
        return TEL1;
    }

    /**
     * @param TEL1 the TEL1 to set
     */
    public void setTEL1(String TEL1) {
        this.TEL1 = TEL1;
    }

    /**
     * @return the TEL2
     */
    public String getTEL2() {
        return TEL2;
    }

    /**
     * @param TEL2 the TEL2 to set
     */
    public void setTEL2(String TEL2) {
        this.TEL2 = TEL2;
    }

    /**
     * @return the ENDERECO
     */
    public String getENDERECO() {
        return ENDERECO;
    }

    /**
     * @param ENDERECO the ENDERECO to set
     */
    public void setENDERECO(String ENDERECO) {
        this.ENDERECO = ENDERECO;
    }

    /**
     * @return the NUMERO
     */
    public String getNUMERO() {
        return NUMERO;
    }

    /**
     * @param NUMERO the NUMERO to set
     */
    public void setNUMERO(String NUMERO) {
        this.NUMERO = NUMERO;
    }

    /**
     * @return the DATA_NASCIMENTO
     */
    public String getDATA_NASCIMENTO() {
        return DATA_NASCIMENTO;
    }

    /**
     * @param DATA_NASCIMENTO the DATA_NASCIMENTO to set
     */
    public void setDATA_NASCIMENTO(String DATA_NASCIMENTO) {
        this.DATA_NASCIMENTO = DATA_NASCIMENTO;
    }

    /**
     * @return the OBS
     */
    public String getOBS() {
        return OBS;
    }

    /**
     * @param OBS the OBS to set
     */
    public void setOBS(String OBS) {
        this.OBS = OBS;
    }

    /**
     * @return the CIDADE
     */
    public String getCIDADE() {
        return CIDADE;
    }

    /**
     * @param CIDADE the CIDADE to set
     */
    public void setCIDADE(String CIDADE) {
        this.CIDADE = CIDADE;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }

    /**
     * @return the GRUPO
     */
    public String getGRUPO() {
        return GRUPO;
    }

    /**
     * @param GRUPO the GRUPO to set
     */
    public void setGRUPO(String GRUPO) {
        this.GRUPO = GRUPO;
    }

    /**
     * @return the VENDEDOR
     */
    public String getVENDEDOR() {
        return VENDEDOR;
    }

    /**
     * @param VENDEDOR the VENDEDOR to set
     */
    public void setVENDEDOR(String VENDEDOR) {
        this.VENDEDOR = VENDEDOR;
    }

}
