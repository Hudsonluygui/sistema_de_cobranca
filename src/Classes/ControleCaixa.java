package Classes;

import CONEXAO.ConexaoOracle;
import Classes.ClasseCaixa;
import Validações.ConverterDecimais;
import Validações.UltimaSequencia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

public class ControleCaixa {

    ConexaoOracle conecta_oracle;
    ConverterDecimais converter;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClasseCaixa Caixa;
    double Saida = 0;
    double Entrada = 0;

    public ControleCaixa() {
        conecta_oracle = new ConexaoOracle();
        Caixa = new ClasseCaixa();
        conecta_oracle.conecta(0);
        converter = new ConverterDecimais();

    }

    public boolean Cadastrar(ClasseCaixa obj) {
        try {
            UltimaSequencia us = new UltimaSequencia("ID_CAIXA", "CAIXA");
            obj.setID_CAIXA(Integer.parseInt(us.getUtl()));
            String SQL = ("INSERT INTO CAIXA(ID_CAIXA, DATA, DESCR, VALOR, OPERACAO)"
                    + "VALUES ("
                    + obj.getID_CAIXA() + ",'"
                    + obj.getDATA() + "','"
                    + obj.getDESCR() + "',"
                    + obj.getVALOR() + ",'"
                    + obj.getOPERACAO() + "')");

            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Caixa.setERRO(String.valueOf(ex));
            JOptionPane.showMessageDialog(null, Caixa.getERRO());
            return false;
        }
    }

    public void Imprimir(String Data_Inicio, String Data_Fim) {
        String SQL = "SELECT C.DESCR, TO_CHAR(C.DATA, 'DD/MM/YYYY') AS DATA, C.VALOR, C.OPERACAO,"
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE OPERACAO = 'SAIDA' AND DATA BETWEEN '" + Data_Inicio + "' AND '"
                + Data_Fim + "' ) AS SAIDA,"
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE OPERACAO = 'ENTRADA' AND DATA BETWEEN '" + Data_Inicio + "' AND '"
                + Data_Fim + "' ) AS ENTRADA"
                + " FROM CAIXA C WHERE DATA BETWEEN '" + Data_Inicio + "' AND '" + Data_Fim + "' ORDER BY C.DATA ASC";

        try {
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Cobrança\\src\\"
                    + "RelCaixa.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void Imprimir(String Descr) {
        String SQL = "SELECT C.DESCR, TO_CHAR(C.DATA, 'DD/MM/YYYY') AS DATA, C.VALOR, C.OPERACAO,"
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE OPERACAO = 'SAIDA' AND DESCR LIKE '%" + Descr + "%') AS SAIDA,"
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE OPERACAO = 'ENTRADA' AND DESCR LIKE '%" + Descr + "%') AS ENTRADA"
                + " FROM CAIXA C WHERE C.DESCR LIKE '%" + Descr+ "%' ORDER BY C.DATA ASC";

        try {
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Cobrança\\src\\"
                    + "RelCaixa.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void ImprimirGeral() {
        String SQL = "SELECT C.DESCR, C.VALOR, TO_CHAR(C.DATA,'DD/MM/YYYY') AS DATA, C.OPERACAO,"
                + "(SELECT SUM(CA.VALOR) FROM CAIXA CA WHERE CA.OPERACAO = 'ENTRADA') AS ENTRADA,"
                + "(SELECT SUM (CX.VALOR) FROM CAIXA CX WHERE CX.OPERACAO = 'SAIDA') AS SAIDA"
                + " FROM CAIXA C";
        try {
            conecta_oracle.executeSQL(SQL);

            JRResultSetDataSource jrRs = new JRResultSetDataSource(conecta_oracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Cobrança\\src\\"
                    + "RelCaixa.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public ResultSet Pesquisar() {
        String SQL = "SELECT DESCR, VALOR,TO_CHAR(DATA,'DD/MM/YYYY') AS DATA, OPERACAO,ID_CAIXA FROM CAIXA"
                + " ORDER BY DESCR ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet Pesquisar(String Descr) {
        String SQL = "SELECT DESCR, VALOR,TO_CHAR(DATA,'DD/MM/YYYY') AS DATA, OPERACAO,ID_CAIXA FROM CAIXA"
                + " WHERE DESCR LIKE '%" + Descr + "%' ";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisarData(String DataInicial, String DataFinal, String Descr) {
        String SQL = "SELECT DESCR, VALOR,TO_CHAR(DATA,'DD/MM/YYYY') AS DATA, OPERACAO,ID_CAIXA FROM CAIXA"
                + " WHERE DATA BETWEEN '" + DataInicial + "' AND '" + DataFinal + "' AND DESCR LIKE '%" + Descr + "%'";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet Pesquisar(String Data_Inicio, String Data_Final) {
        String SQL = "SELECT DESCR, VALOR,TO_CHAR(DATA,'DD/MM/YYYY') AS DATA, "
                + " OPERACAO,FORMA,ID_CAIXA FROM CAIXA "
                + " WHERE DATA BETWEEN '" + Data_Inicio + "' AND '" + Data_Final + "'"
                + " ORDER BY DESCR ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public boolean Excluir(ClasseCaixa obj) {
        // ControleOrdemAssociativa Associativa = new ControleOrdemAssociativa();
        String SQL = "DELETE FROM CAIXA WHERE ID_CAIXA = " + obj.getID_CAIXA();
        try {
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public String Ano() {
        String SQL = "SELECT TO_CHAR(SYSDATE,'YYYY') AS DATA FROM DUAL";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            SQL = conecta_oracle.resultSet.getString("DATA");
        } catch (Exception e) {
            SQL = "";
        }
        return SQL;
    }

    public String Dia() {
        String SQL = "SELECT TO_CHAR(SYSDATE,'DD') AS DATA FROM DUAL";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            SQL = conecta_oracle.resultSet.getString("DATA");
        } catch (Exception e) {
            SQL = "";
        }
        return SQL;
    }

    public String Mes() {
        String SQL = "SELECT TO_CHAR(SYSDATE,'MM') AS DATA FROM DUAL";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            SQL = conecta_oracle.resultSet.getString("DATA");
        } catch (Exception e) {
            SQL = "";
        }
        return SQL;
    }

    public String Data() {
        String SQL = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') AS DATA FROM DUAL";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            SQL = conecta_oracle.resultSet.getString("DATA");
        } catch (Exception e) {
            SQL = "";
        }
        return SQL;
    }

    public void Janeiro(String ANO) {
        String SQL = "SELECT C.DESCR, TO_CHAR(C.DATA, 'DD/MM/YYYY') AS DATA,C.VALOR, C.OPERACAO,C.FORMA, "
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE OPERACAO = 'SAIDA' "
                + " AND DATA BETWEEN '01/01/" + ANO + "' AND '31/01/" + ANO + "') AS SAIDA ,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/01/" + ANO + "' AND '31/01/" + ANO + "') AS ENTRADA, "
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO'"
                + " AND DATA BETWEEN '01/01/" + ANO + "' AND '31/01/" + ANO + "') AS DINHEIRO,"
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'CHEQUE' "
                + " AND DATA BETWEEN '01/01/" + ANO + "' AND '31/01/" + ANO + "') AS CHEQUE,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/01/" + ANO + "' AND '31/01/" + ANO + "') AS ENTRADA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'ENTRADA'"
                + " AND DATA BETWEEN '01/01/" + ANO + "' AND '31/01/" + ANO + "') AS ENTRADA_CHEQUE,"
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/01/" + ANO + "' AND '31/01/" + ANO + "') AS SAIDA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/01/" + ANO + "' AND '31/01/" + ANO + "') AS SAIDA_CHEQUE"
                + " FROM CAIXA C "
                + " WHERE DATA BETWEEN '01/01/" + ANO + "' AND '31/01/" + ANO + "'";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();

            Entrada = conecta_oracle.resultSet.getDouble("ENTRADA");
            Saida = conecta_oracle.resultSet.getDouble("SAIDA");
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        try {
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\SistemaRetifica_WESLEY\\src\\Relatorio\\"
                    + "Caixa.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);

            viewer.setVisible(true);
            Entrada = Entrada - Saida;
            Entrada = converter.converterDoubleDoisDecimais(Entrada);
            if (Entrada > 0) {
                JOptionPane.showMessageDialog(null, "Lucro de: R$ " + Entrada);
            } else if (Entrada < 0) {
                JOptionPane.showMessageDialog(null, "Prejuizo de: R$ " + Entrada);
            } else if (Entrada == 0) {
                JOptionPane.showMessageDialog(null, "Sem Lucro, Nem Prejuizo");
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void Fevereiro(String ANO) {

        String SQL = "SELECT C.DESCR, TO_CHAR(C.DATA, 'DD/MM/YYYY') AS DATA,C.VALOR, C.OPERACAO,C.FORMA, "
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE OPERACAO = 'SAIDA' "
                + " AND DATA BETWEEN '01/02/" + ANO + "' AND '28/02/" + ANO + "') AS SAIDA ,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/02/" + ANO + "' AND '28/02/" + ANO + "') AS ENTRADA, "
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO'"
                + " AND DATA BETWEEN '01/02/" + ANO + "' AND '28/02/" + ANO + "') AS DINHEIRO,"
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'CHEQUE' "
                + " AND DATA BETWEEN '01/02/" + ANO + "' AND '28/02/" + ANO + "') AS CHEQUE,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/02/" + ANO + "' AND '28/02/" + ANO + "') AS ENTRADA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'ENTRADA'"
                + " AND DATA BETWEEN '01/02/" + ANO + "' AND '28/02/" + ANO + "') AS ENTRADA_CHEQUE,"
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/02/" + ANO + "' AND '28/02/" + ANO + "') AS SAIDA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/02/" + ANO + "' AND '28/02/" + ANO + "') AS SAIDA_CHEQUE"
                + " FROM CAIXA C "
                + " WHERE DATA BETWEEN '01/02/" + ANO + "' AND '28/02/" + ANO + "'";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Entrada = conecta_oracle.resultSet.getDouble("ENTRADA");
            Saida = conecta_oracle.resultSet.getDouble("SAIDA");
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        try {
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\SistemaRetifica_WESLEY\\src\\Relatorio\\"
                    + "Caixa.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);

            viewer.setVisible(true);
            Entrada = Entrada - Saida;
            Entrada = converter.converterDoubleDoisDecimais(Entrada);
            if (Entrada > 0) {
                JOptionPane.showMessageDialog(null, "Lucro de: R$ " + Entrada);
            } else if (Entrada < 0) {
                JOptionPane.showMessageDialog(null, "Prejuizo de: R$ " + Entrada);
            } else if (Entrada == 0) {
                JOptionPane.showMessageDialog(null, "Sem Lucro, Nem Prejuizo");
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void Março(String ANO) {

        String SQL = "SELECT C.DESCR, TO_CHAR(C.DATA, 'DD/MM/YYYY') AS DATA,C.VALOR, C.OPERACAO,C.FORMA, "
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE OPERACAO = 'SAIDA' "
                + " AND DATA BETWEEN '01/03/" + ANO + "' AND '31/03/" + ANO + "') AS SAIDA ,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/03/" + ANO + "' AND '31/03/" + ANO + "') AS ENTRADA, "
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO'"
                + " AND DATA BETWEEN '01/03/" + ANO + "' AND '31/03/" + ANO + "') AS DINHEIRO,"
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'CHEQUE' "
                + " AND DATA BETWEEN '01/03/" + ANO + "' AND '31/03/" + ANO + "') AS CHEQUE,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/03/" + ANO + "' AND '31/03/" + ANO + "') AS ENTRADA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'ENTRADA'"
                + " AND DATA BETWEEN '01/03/" + ANO + "' AND '31/03/" + ANO + "') AS ENTRADA_CHEQUE,"
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/03/" + ANO + "' AND '31/03/" + ANO + "') AS SAIDA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/03/" + ANO + "' AND '31/03/" + ANO + "') AS SAIDA_CHEQUE"
                + " FROM CAIXA C "
                + " WHERE DATA BETWEEN '01/03/" + ANO + "' AND '31/03/" + ANO + "'";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Entrada = conecta_oracle.resultSet.getDouble("ENTRADA");
            Saida = conecta_oracle.resultSet.getDouble("SAIDA");

        } catch (SQLException ex) {
            System.out.println(ex);
        }
        try {
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\SistemaRetifica_WESLEY\\src\\Relatorio\\"
                    + "Caixa.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);

            viewer.setVisible(true);
            Entrada = Entrada - Saida;
            Entrada = converter.converterDoubleDoisDecimais(Entrada);
            if (Entrada > 0) {
                JOptionPane.showMessageDialog(null, "Lucro de: R$ " + Entrada);
            } else if (Entrada < 0) {
                JOptionPane.showMessageDialog(null, "Prejuizo de: R$ " + Entrada);
            } else if (Entrada == 0) {
                JOptionPane.showMessageDialog(null, "Sem Lucro, Nem Prejuizo");
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void Abril(String ANO) {

        String SQL = "SELECT C.DESCR, TO_CHAR(C.DATA, 'DD/MM/YYYY') AS DATA,C.VALOR, C.OPERACAO,C.FORMA, "
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE OPERACAO = 'SAIDA' "
                + " AND DATA BETWEEN '01/04/" + ANO + "' AND '30/04/" + ANO + "') AS SAIDA ,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/04/" + ANO + "' AND '30/04/" + ANO + "') AS ENTRADA, "
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO'"
                + " AND DATA BETWEEN '01/04/" + ANO + "' AND '30/04/" + ANO + "') AS DINHEIRO,"
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'CHEQUE' "
                + " AND DATA BETWEEN '01/04/" + ANO + "' AND '30/04/" + ANO + "') AS CHEQUE,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/04/" + ANO + "' AND '30/04/" + ANO + "') AS ENTRADA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'ENTRADA'"
                + " AND DATA BETWEEN '01/04/" + ANO + "' AND '30/04/" + ANO + "') AS ENTRADA_CHEQUE,"
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/04/" + ANO + "' AND '30/04/" + ANO + "') AS SAIDA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/04/" + ANO + "' AND '30/04/" + ANO + "') AS SAIDA_CHEQUE"
                + " FROM CAIXA C "
                + " WHERE DATA BETWEEN '01/04/" + ANO + "' AND '30/04/" + ANO + "'";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Entrada = conecta_oracle.resultSet.getDouble("ENTRADA");
            Saida = conecta_oracle.resultSet.getDouble("SAIDA");
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        try {
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\SistemaRetifica_WESLEY\\src\\Relatorio\\"
                    + "Caixa.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);

            viewer.setVisible(true);
            Entrada = Entrada - Saida;
            Entrada = converter.converterDoubleDoisDecimais(Entrada);
            if (Entrada > 0) {
                JOptionPane.showMessageDialog(null, "Lucro de: R$ " + Entrada);
            } else if (Entrada < 0) {
                JOptionPane.showMessageDialog(null, "Prejuizo de: R$ " + Entrada);
            } else if (Entrada == 0) {
                JOptionPane.showMessageDialog(null, "Sem Lucro, Nem Prejuizo");
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void Maio(String ANO) {

        String SQL = "SELECT C.DESCR, TO_CHAR(C.DATA, 'DD/MM/YYYY') AS DATA,C.VALOR, C.OPERACAO,C.FORMA, "
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE OPERACAO = 'SAIDA' "
                + " AND DATA BETWEEN '01/05/" + ANO + "' AND '31/05/" + ANO + "') AS SAIDA ,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/05/" + ANO + "' AND '31/05/" + ANO + "') AS ENTRADA, "
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO'"
                + " AND DATA BETWEEN '01/05/" + ANO + "' AND '31/05/" + ANO + "') AS DINHEIRO,"
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'CHEQUE' "
                + " AND DATA BETWEEN '01/05/" + ANO + "' AND '31/05/" + ANO + "') AS CHEQUE,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/05/" + ANO + "' AND '31/05/" + ANO + "') AS ENTRADA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'ENTRADA'"
                + " AND DATA BETWEEN '01/05/" + ANO + "' AND '31/05/" + ANO + "') AS ENTRADA_CHEQUE,"
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/05/" + ANO + "' AND '31/05/" + ANO + "') AS SAIDA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/05/" + ANO + "' AND '31/05/" + ANO + "') AS SAIDA_CHEQUE"
                + " FROM CAIXA C "
                + " WHERE DATA BETWEEN '01/05/" + ANO + "' AND '31/05/" + ANO + "'";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Entrada = conecta_oracle.resultSet.getDouble("ENTRADA");
            Saida = conecta_oracle.resultSet.getDouble("SAIDA");
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        try {
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\SistemaRetifica_WESLEY\\src\\Relatorio\\"
                    + "Caixa.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);

            viewer.setVisible(true);
            Entrada = Entrada - Saida;
            Entrada = converter.converterDoubleDoisDecimais(Entrada);
            if (Entrada > 0) {
                JOptionPane.showMessageDialog(null, "Lucro de: R$ " + Entrada);
            } else if (Entrada < 0) {
                JOptionPane.showMessageDialog(null, "Prejuizo de: R$ " + Entrada);
            } else if (Entrada == 0) {
                JOptionPane.showMessageDialog(null, "Sem Lucro, Nem Prejuizo");
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void Junho(String ANO) {

        String SQL = "SELECT C.DESCR, TO_CHAR(C.DATA, 'DD/MM/YYYY') AS DATA,C.VALOR, C.OPERACAO,C.FORMA, "
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE OPERACAO = 'SAIDA' "
                + " AND DATA BETWEEN '01/06/" + ANO + "' AND '30/06/" + ANO + "') AS SAIDA ,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/06/" + ANO + "' AND '30/06/" + ANO + "') AS ENTRADA, "
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO'"
                + " AND DATA BETWEEN '01/06/" + ANO + "' AND '30/06/" + ANO + "') AS DINHEIRO,"
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'CHEQUE' "
                + " AND DATA BETWEEN '01/06/" + ANO + "' AND '30/06/" + ANO + "') AS CHEQUE,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/06/" + ANO + "' AND '30/06/" + ANO + "') AS ENTRADA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'ENTRADA'"
                + " AND DATA BETWEEN '01/06/" + ANO + "' AND '30/06/" + ANO + "') AS ENTRADA_CHEQUE,"
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/06/" + ANO + "' AND '30/06/" + ANO + "') AS SAIDA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/06/" + ANO + "' AND '30/06/" + ANO + "') AS SAIDA_CHEQUE"
                + " FROM CAIXA C "
                + " WHERE DATA BETWEEN '01/06/" + ANO + "' AND '30/06/" + ANO + "'";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Entrada = conecta_oracle.resultSet.getDouble("ENTRADA");
            Saida = conecta_oracle.resultSet.getDouble("SAIDA");
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        try {
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\SistemaRetifica_WESLEY\\src\\Relatorio\\"
                    + "Caixa.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);

            viewer.setVisible(true);
            Entrada = Entrada - Saida;
            Entrada = converter.converterDoubleDoisDecimais(Entrada);
            if (Entrada > 0) {
                JOptionPane.showMessageDialog(null, "Lucro de: R$ " + Entrada);
            } else if (Entrada < 0) {
                JOptionPane.showMessageDialog(null, "Prejuizo de: R$ " + Entrada);
            } else if (Entrada == 0) {
                JOptionPane.showMessageDialog(null, "Sem Lucro, Nem Prejuizo");
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void Julho(String ANO) {

        String SQL = "SELECT C.DESCR, TO_CHAR(C.DATA, 'DD/MM/YYYY') AS DATA,C.VALOR, C.OPERACAO,C.FORMA, "
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE OPERACAO = 'SAIDA' "
                + " AND DATA BETWEEN '01/07/" + ANO + "' AND '31/07/" + ANO + "') AS SAIDA ,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/07/" + ANO + "' AND '31/07/" + ANO + "') AS ENTRADA, "
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO'"
                + " AND DATA BETWEEN '01/07/" + ANO + "' AND '31/07/" + ANO + "') AS DINHEIRO,"
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'CHEQUE' "
                + " AND DATA BETWEEN '01/07/" + ANO + "' AND '31/07/" + ANO + "') AS CHEQUE,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/07/" + ANO + "' AND '31/07/" + ANO + "') AS ENTRADA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'ENTRADA'"
                + " AND DATA BETWEEN '01/07/" + ANO + "' AND '31/07/" + ANO + "') AS ENTRADA_CHEQUE,"
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/07/" + ANO + "' AND '31/07/" + ANO + "') AS SAIDA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/07/" + ANO + "' AND '31/07/" + ANO + "') AS SAIDA_CHEQUE"
                + " FROM CAIXA C "
                + " WHERE DATA BETWEEN '01/07/" + ANO + "' AND '31/07/" + ANO + "'";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Entrada = conecta_oracle.resultSet.getDouble("ENTRADA");
            Saida = conecta_oracle.resultSet.getDouble("SAIDA");
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        try {
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\SistemaRetifica_WESLEY\\src\\Relatorio\\"
                    + "Caixa.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);

            viewer.setVisible(true);
            Entrada = Entrada - Saida;
            Entrada = converter.converterDoubleDoisDecimais(Entrada);
            if (Entrada > 0) {
                JOptionPane.showMessageDialog(null, "Lucro de: R$ " + Entrada);
            } else if (Entrada < 0) {
                JOptionPane.showMessageDialog(null, "Prejuizo de: R$ " + Entrada);
            } else if (Entrada == 0) {
                JOptionPane.showMessageDialog(null, "Sem Lucro, Nem Prejuizo");
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void Agosto(String ANO) {

        String SQL = "SELECT C.DESCR, TO_CHAR(C.DATA, 'DD/MM/YYYY') AS DATA,C.VALOR, C.OPERACAO,C.FORMA, "
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE OPERACAO = 'SAIDA' "
                + " AND DATA BETWEEN '01/08/" + ANO + "' AND '31/08/" + ANO + "') AS SAIDA ,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/08/" + ANO + "' AND '31/08/" + ANO + "') AS ENTRADA, "
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO'"
                + " AND DATA BETWEEN '01/08/" + ANO + "' AND '31/08/" + ANO + "') AS DINHEIRO,"
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'CHEQUE' "
                + " AND DATA BETWEEN '01/08/" + ANO + "' AND '31/08/" + ANO + "') AS CHEQUE,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/08/" + ANO + "' AND '31/08/" + ANO + "') AS ENTRADA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'ENTRADA'"
                + " AND DATA BETWEEN '01/08/" + ANO + "' AND '31/08/" + ANO + "') AS ENTRADA_CHEQUE,"
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/08/" + ANO + "' AND '31/08/" + ANO + "') AS SAIDA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/08/" + ANO + "' AND '31/08/" + ANO + "') AS SAIDA_CHEQUE"
                + " FROM CAIXA C "
                + " WHERE DATA BETWEEN '01/08/" + ANO + "' AND '31/08/" + ANO + "'";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Entrada = conecta_oracle.resultSet.getDouble("ENTRADA");
            Saida = conecta_oracle.resultSet.getDouble("SAIDA");
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        try {
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\SistemaRetifica_WESLEY\\src\\Relatorio\\"
                    + "Caixa.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);

            viewer.setVisible(true);
            Entrada = Entrada - Saida;
            Entrada = converter.converterDoubleDoisDecimais(Entrada);
            if (Entrada > 0) {
                JOptionPane.showMessageDialog(null, "Lucro de: R$ " + Entrada);
            } else if (Entrada < 0) {
                JOptionPane.showMessageDialog(null, "Prejuizo de: R$ " + Entrada);
            } else if (Entrada == 0) {
                JOptionPane.showMessageDialog(null, "Sem Lucro, Nem Prejuizo");
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void Setembro(String ANO) {

        String SQL = "SELECT C.DESCR, TO_CHAR(C.DATA, 'DD/MM/YYYY') AS DATA,C.VALOR, C.OPERACAO,C.FORMA, "
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE OPERACAO = 'SAIDA' "
                + " AND DATA BETWEEN '01/09/" + ANO + "' AND '30/09/" + ANO + "') AS SAIDA ,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/09/" + ANO + "' AND '30/09/" + ANO + "') AS ENTRADA, "
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO'"
                + " AND DATA BETWEEN '01/09/" + ANO + "' AND '30/09/" + ANO + "') AS DINHEIRO,"
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'CHEQUE' "
                + " AND DATA BETWEEN '01/09/" + ANO + "' AND '30/09/" + ANO + "') AS CHEQUE,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/09/" + ANO + "' AND '30/09/" + ANO + "') AS ENTRADA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'ENTRADA'"
                + " AND DATA BETWEEN '01/09/" + ANO + "' AND '30/09/" + ANO + "') AS ENTRADA_CHEQUE,"
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/09/" + ANO + "' AND '30/09/" + ANO + "') AS SAIDA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/09/" + ANO + "' AND '30/09/" + ANO + "') AS SAIDA_CHEQUE"
                + " FROM CAIXA C "
                + " WHERE DATA BETWEEN '01/09/" + ANO + "' AND '30/09/" + ANO + "'";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Entrada = conecta_oracle.resultSet.getDouble("ENTRADA");
            Saida = conecta_oracle.resultSet.getDouble("SAIDA");
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        try {
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\SistemaRetifica_WESLEY\\src\\Relatorio\\"
                    + "Caixa.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);

            viewer.setVisible(true);
            Entrada = Entrada - Saida;
            Entrada = converter.converterDoubleDoisDecimais(Entrada);
            if (Entrada > 0) {
                JOptionPane.showMessageDialog(null, "Lucro de: R$ " + Entrada);
            } else if (Entrada < 0) {
                JOptionPane.showMessageDialog(null, "Prejuizo de: R$ " + Entrada);
            } else if (Entrada == 0) {
                JOptionPane.showMessageDialog(null, "Sem Lucro, Nem Prejuizo");
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void Outubro(String ANO) {

        String SQL = "SELECT C.DESCR, TO_CHAR(C.DATA, 'DD/MM/YYYY') AS DATA,C.VALOR, C.OPERACAO,C.FORMA, "
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE OPERACAO = 'SAIDA' "
                + " AND DATA BETWEEN '01/10/" + ANO + "' AND '31/10/" + ANO + "') AS SAIDA ,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/10/" + ANO + "' AND '31/10/" + ANO + "') AS ENTRADA, "
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO'"
                + " AND DATA BETWEEN '01/10/" + ANO + "' AND '31/010/" + ANO + "') AS DINHEIRO,"
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'CHEQUE' "
                + " AND DATA BETWEEN '01/10/" + ANO + "' AND '31/10/" + ANO + "') AS CHEQUE,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/10/" + ANO + "' AND '31/10/" + ANO + "') AS ENTRADA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'ENTRADA'"
                + " AND DATA BETWEEN '01/10/" + ANO + "' AND '31/10/" + ANO + "') AS ENTRADA_CHEQUE,"
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/10/" + ANO + "' AND '31/10/" + ANO + "') AS SAIDA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/10/" + ANO + "' AND '31/10/" + ANO + "') AS SAIDA_CHEQUE"
                + " FROM CAIXA C "
                + " WHERE DATA BETWEEN '01/10/" + ANO + "' AND '31/10/" + ANO + "'";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Entrada = conecta_oracle.resultSet.getDouble("ENTRADA");
            Saida = conecta_oracle.resultSet.getDouble("SAIDA");
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        try {
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\SistemaRetifica_WESLEY\\src\\Relatorio\\"
                    + "Caixa.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);

            viewer.setVisible(true);
            Entrada = Entrada - Saida;
            Entrada = converter.converterDoubleDoisDecimais(Entrada);
            if (Entrada > 0) {
                JOptionPane.showMessageDialog(null, "Lucro de: R$ " + Entrada);
            } else if (Entrada < 0) {
                JOptionPane.showMessageDialog(null, "Prejuizo de: R$ " + Entrada);
            } else if (Entrada == 0) {
                JOptionPane.showMessageDialog(null, "Sem Lucro, Nem Prejuizo");
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void Novembro(String ANO) {

        String SQL = "SELECT C.DESCR, TO_CHAR(C.DATA, 'DD/MM/YYYY') AS DATA,C.VALOR, C.OPERACAO,C.FORMA, "
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE OPERACAO = 'SAIDA' "
                + " AND DATA BETWEEN '01/11/" + ANO + "' AND '30/11/" + ANO + "') AS SAIDA ,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/11/" + ANO + "' AND '30/11/" + ANO + "') AS ENTRADA, "
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO'"
                + " AND DATA BETWEEN '01/11/" + ANO + "' AND '30/11/" + ANO + "') AS DINHEIRO,"
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'CHEQUE' "
                + " AND DATA BETWEEN '01/11/" + ANO + "' AND '30/11/" + ANO + "') AS CHEQUE,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/11/" + ANO + "' AND '30/11/" + ANO + "') AS ENTRADA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'ENTRADA'"
                + " AND DATA BETWEEN '01/11/" + ANO + "' AND '30/11/" + ANO + "') AS ENTRADA_CHEQUE,"
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/11/" + ANO + "' AND '30/11/" + ANO + "') AS SAIDA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/11/" + ANO + "' AND '30/11/" + ANO + "') AS SAIDA_CHEQUE"
                + " FROM CAIXA C "
                + " WHERE DATA BETWEEN '01/11/" + ANO + "' AND '30/11/" + ANO + "'";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Entrada = conecta_oracle.resultSet.getDouble("ENTRADA");
            Saida = conecta_oracle.resultSet.getDouble("SAIDA");
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        try {
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\SistemaRetifica_WESLEY\\src\\Relatorio\\"
                    + "Caixa.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);

            viewer.setVisible(true);
            Entrada = Entrada - Saida;
            Entrada = converter.converterDoubleDoisDecimais(Entrada);
            if (Entrada > 0) {
                JOptionPane.showMessageDialog(null, "Lucro de: R$ " + Entrada);
            } else if (Entrada < 0) {
                JOptionPane.showMessageDialog(null, "Prejuizo de: R$ " + Entrada);
            } else if (Entrada == 0) {
                JOptionPane.showMessageDialog(null, "Sem Lucro, Nem Prejuizo");
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void Dezembro(String ANO) {

        String SQL = "SELECT C.DESCR, TO_CHAR(C.DATA, 'DD/MM/YYYY') AS DATA,C.VALOR, C.OPERACAO,C.FORMA, "
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE OPERACAO = 'SAIDA' "
                + " AND DATA BETWEEN '01/12/" + ANO + "' AND '31/12/" + ANO + "') AS SAIDA ,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/12/" + ANO + "' AND '31/12/" + ANO + "') AS ENTRADA, "
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO'"
                + " AND DATA BETWEEN '01/12/" + ANO + "' AND '31/12/" + ANO + "') AS DINHEIRO,"
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'CHEQUE' "
                + " AND DATA BETWEEN '01/12/" + ANO + "' AND '31/12/" + ANO + "') AS CHEQUE,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/12/" + ANO + "' AND '31/12/" + ANO + "') AS ENTRADA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'ENTRADA'"
                + " AND DATA BETWEEN '01/12/" + ANO + "' AND '31/12/" + ANO + "') AS ENTRADA_CHEQUE,"
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/12/" + ANO + "' AND '31/12/" + ANO + "') AS SAIDA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/12/" + ANO + "' AND '31/12/" + ANO + "') AS SAIDA_CHEQUE"
                + " FROM CAIXA C "
                + " WHERE DATA BETWEEN '01/12/" + ANO + "' AND '31/12/" + ANO + "'";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Entrada = conecta_oracle.resultSet.getDouble("ENTRADA");
            Saida = conecta_oracle.resultSet.getDouble("SAIDA");
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        try {
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\SistemaRetifica_WESLEY\\src\\Relatorio\\"
                    + "Caixa.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);

            viewer.setVisible(true);
            Entrada = Entrada - Saida;
            Entrada = converter.converterDoubleDoisDecimais(Entrada);
            if (Entrada > 0) {
                JOptionPane.showMessageDialog(null, "Lucro de: R$ " + Entrada);
            } else if (Entrada < 0) {
                JOptionPane.showMessageDialog(null, "Prejuizo de: R$ " + Entrada);
            } else if (Entrada == 0) {
                JOptionPane.showMessageDialog(null, "Sem Lucro, Nem Prejuizo");
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void Todos(String ANO) {
        String SQL = "SELECT C.DESCR, TO_CHAR(C.DATA, 'DD/MM/YYYY') AS DATA,C.VALOR, C.OPERACAO,C.FORMA, "
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE OPERACAO = 'SAIDA' "
                + " AND DATA BETWEEN '01/01/" + ANO + "' AND '31/12/" + ANO + "') AS SAIDA ,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/01/" + ANO + "' AND '31/12/" + ANO + "') AS ENTRADA, "
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO'"
                + " AND DATA BETWEEN '01/01/" + ANO + "' AND '31/12/" + ANO + "') AS DINHEIRO,"
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'CHEQUE' "
                + " AND DATA BETWEEN '01/01/" + ANO + "' AND '31/12/" + ANO + "') AS CHEQUE,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '01/01/" + ANO + "' AND '31/12/" + ANO + "') AS ENTRADA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'ENTRADA'"
                + " AND DATA BETWEEN '01/01/" + ANO + "' AND '31/12/" + ANO + "') AS ENTRADA_CHEQUE,"
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/01/" + ANO + "' AND '31/12/" + ANO + "') AS SAIDA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '01/01/" + ANO + "' AND '31/12/" + ANO + "') AS SAIDA_CHEQUE"
                + " FROM CAIXA C "
                + " WHERE DATA BETWEEN '01/01/" + ANO + "' AND '31/12/" + ANO + "'"
                + " ORDER BY DATA ASC";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Entrada = conecta_oracle.resultSet.getDouble("ENTRADA");
            Saida = conecta_oracle.resultSet.getDouble("SAIDA");
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        try {
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\SistemaRetifica_WESLEY\\src\\Relatorio\\"
                    + "Caixa.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);

            viewer.setVisible(true);
            Entrada = Entrada - Saida;
            Entrada = converter.converterDoubleDoisDecimais(Entrada);
            if (Entrada > 0) {
                JOptionPane.showMessageDialog(null, "Lucro de: R$ " + Entrada);
            } else if (Entrada < 0) {
                JOptionPane.showMessageDialog(null, "Prejuizo de: R$ " + Entrada);
            } else if (Entrada == 0) {
                JOptionPane.showMessageDialog(null, "Sem Lucro, Nem Prejuizo");
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void Diario(String ANO, String Mes, String Dia) {
        String SQL = "SELECT C.DESCR, TO_CHAR(C.DATA, 'DD/MM/YYYY') AS DATA,C.VALOR, C.OPERACAO,C.FORMA, "
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE OPERACAO = 'SAIDA' "
                + " AND DATA = '" + Dia + "/" + Mes + "/" + ANO + "') AS SAIDA ,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE OPERACAO = 'ENTRADA' "
                + " AND DATA = '" + Dia + "/" + Mes + "/" + ANO + "') AS ENTRADA ,"
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO'"
                + " AND DATA = '" + Dia + "/" + Mes + "/" + ANO + "') AS DINHEIRO ,"
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'CHEQUE' "
                + " AND DATA = '" + Dia + "/" + Mes + "/" + ANO + "') AS CHEQUE ,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'ENTRADA' "
                + " AND DATA = '" + Dia + "/" + Mes + "/" + ANO + "') AS ENTRADA_DINHEIRO ,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'ENTRADA'"
                + " AND DATA = '" + Dia + "/" + Mes + "/" + ANO + "') AS ENTRADA_CHEQUE ,"
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'SAIDA'"
                + " AND DATA = '" + Dia + "/" + Mes + "/" + ANO + "') AS SAIDA_DINHEIRO ,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'SAIDA'"
                + " AND DATA = '" + Dia + "/" + Mes + "/" + ANO + "') AS SAIDA_CHEQUE"
                + " FROM CAIXA C "
                + " WHERE DATA = '" + Dia + "/" + Mes + "/" + ANO + "'"
                + " ORDER BY OPERACAO ASC";

        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Entrada = conecta_oracle.resultSet.getDouble("ENTRADA");
            Saida = conecta_oracle.resultSet.getDouble("SAIDA");
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        try {
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\SistemaRetifica_WESLEY\\src\\Relatorio\\"
                    + "Caixa.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);

            viewer.setVisible(true);
            Entrada = Entrada - Saida;
            Entrada = converter.converterDoubleDoisDecimais(Entrada);
            if (Entrada > 0) {
                JOptionPane.showMessageDialog(null, "Lucro de: R$ " + Entrada);
            } else if (Entrada < 0) {
                JOptionPane.showMessageDialog(null, "Prejuizo de: R$ " + Entrada);
            } else if (Entrada == 0) {
                JOptionPane.showMessageDialog(null, "Sem Lucro, Nem Prejuizo");
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public String PegarDia() {
        String SQL = "SELECT TO_CHAR(SYSDATE,'DD') AS DIA FROM DUAL";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            SQL = conecta_oracle.resultSet.getString("DIA");
        } catch (Exception e) {
            SQL = "";
        }
        return SQL;
    }

    public String PegarMes() {
        String SQL = "SELECT TO_CHAR(SYSDATE,'MM') AS MES FROM DUAL";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            SQL = conecta_oracle.resultSet.getString("MES");
        } catch (Exception e) {
            SQL = "";
        }
        return SQL;
    }

    public void SEMANAL(String ANO, String Mes, String Dia, String Dia_Ant) {
        String SQL = "SELECT C.DESCR, TO_CHAR(C.DATA, 'DD/MM/YYYY') AS DATA,C.VALOR, C.OPERACAO,C.FORMA, "
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE OPERACAO = 'SAIDA' "
                + " AND DATA BETWEEN '" + Dia_Ant + "/" + Mes + "/" + ANO + "' AND '" + Dia + "/" + Mes + "/" + ANO + "') AS SAIDA ,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '" + Dia_Ant + "/" + Mes + "/" + ANO + "' AND '" + Dia + "/" + Mes + "/" + ANO + "') AS ENTRADA, "
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO'"
                + " AND DATA BETWEEN '" + Dia_Ant + "/" + Mes + "/" + ANO + "' AND '" + Dia + "/" + Mes + "/" + ANO + "') AS DINHEIRO,"
                + "(SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'CHEQUE' "
                + " AND DATA BETWEEN '" + Dia_Ant + "/" + Mes + "/" + ANO + "' AND '" + Dia + "/" + Mes + "/" + ANO + "') AS CHEQUE,"
                + " (SELECT SUM(VALOR)FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'ENTRADA' "
                + " AND DATA BETWEEN '" + Dia_Ant + "/" + Mes + "/" + ANO + "' AND '" + Dia + "/" + Mes + "/" + ANO + "') AS ENTRADA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'ENTRADA'"
                + " AND DATA BETWEEN '" + Dia_Ant + "/" + Mes + "/" + ANO + "' AND '" + Dia + "/" + Mes + "/" + ANO + "') AS ENTRADA_CHEQUE,"
                + "(SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'DINHEIRO' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '" + Dia_Ant + "/" + Mes + "/" + ANO + "' AND '" + Dia + "/" + Mes + "/" + ANO + "') AS SAIDA_DINHEIRO,"
                + " (SELECT SUM(VALOR) FROM CAIXA WHERE FORMA = 'CHEQUE' AND OPERACAO = 'SAIDA'"
                + " AND DATA BETWEEN '" + Dia_Ant + "/" + Mes + "/" + ANO + "' AND '" + Dia + "/" + Mes + "/" + ANO + "') AS SAIDA_CHEQUE"
                + " FROM CAIXA C "
                + " WHERE DATA BETWEEN '" + Dia_Ant + "/" + Mes + "/" + ANO + "' AND '" + Dia + "/" + Mes + "/" + ANO + "'"
                + " ORDER BY DATA ASC";

        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Entrada = conecta_oracle.resultSet.getDouble("ENTRADA");
            Saida = conecta_oracle.resultSet.getDouble("SAIDA");
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        try {
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\SistemaRetifica_WESLEY\\src\\Relatorio\\"
                    + "Caixa.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);

            viewer.setVisible(true);
            Entrada = Entrada - Saida;
            Entrada = converter.converterDoubleDoisDecimais(Entrada);
            if (Entrada > 0) {
                JOptionPane.showMessageDialog(null, "Lucro de: R$ " + Entrada);
            } else if (Entrada < 0) {
                JOptionPane.showMessageDialog(null, "Prejuizo de: R$ " + Entrada);
            } else if (Entrada == 0) {
                JOptionPane.showMessageDialog(null, "Sem Lucro, Nem Prejuizo");
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
