
package Classes;




public class ClasseCliente {
private int ID_CLIENTE;
private String SITUACAO;
private Double VALOR;
private String NOME;
private String TEL1;
private String TEL2;
private String DATA_ATUAL;
private String DATA_VENCIMENTO;
private String CIDADE;
private String ERRO;
private String DATA_PAGAMENTO;


    /**
     * @return the ID_CLIENTE
     */
    public int getID_CLIENTE() {
        return ID_CLIENTE;
    }

    /**
     * @param ID_CLIENTE the ID_CLIENTE to set
     */
    public void setID_CLIENTE(int ID_CLIENTE) {
        this.ID_CLIENTE = ID_CLIENTE;
    }

    /**
     * @return the VALOR
     */
    public Double getVALOR() {
        return VALOR;
    }

    /**
     * @param VALOR the VALOR to set
     */
    public void setVALOR(Double VALOR) {
        this.VALOR = VALOR;
    }

    /**
     * @return the NOME
     */
    public String getNOME() {
        return NOME;
    }

    /**
     * @param NOME the NOME to set
     */
    public void setNOME(String NOME) {
        this.NOME = NOME;
    }

    /**
     * @return the TEL1
     */
    public String getTEL1() {
        return TEL1;
    }

    /**
     * @param TEL1 the TEL1 to set
     */
    public void setTEL1(String TEL1) {
        this.TEL1 = TEL1;
    }

    /**
     * @return the TEL2
     */
    public String getTEL2() {
        return TEL2;
    }

    /**
     * @param TEL2 the TEL2 to set
     */
    public void setTEL2(String TEL2) {
        this.TEL2 = TEL2;
    }

    /**
     * @return the DATA_ATUAL
     */
    public String getDATA_ATUAL() {
        return DATA_ATUAL;
    }

    /**
     * @param DATA_ATUAL the DATA_ATUAL to set
     */
    public void setDATA_ATUAL(String DATA_ATUAL) {
        this.DATA_ATUAL = DATA_ATUAL;
    }

    /**
     * @return the DATA_VENCIMENTO
     */
    public String getDATA_VENCIMENTO() {
        return DATA_VENCIMENTO;
    }

    /**
     * @param DATA_VENCIMENTO the DATA_VENCIMENTO to set
     */
    public void setDATA_VENCIMENTO(String DATA_VENCIMENTO) {
        this.DATA_VENCIMENTO = DATA_VENCIMENTO;
    }

    /**
     * @return the CIDADE
     */
    public String getCIDADE() {
        return CIDADE;
    }

    /**
     * @param CIDADE the CIDADE to set
     */
    public void setCIDADE(String CIDADE) {
        this.CIDADE = CIDADE;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }

    /**
     * @return the SITUACAO
     */
    public String getSITUACAO() {
        return SITUACAO;
    }

    /**
     * @param SITUACAO the SITUACAO to set
     */
    public void setSITUACAO(String SITUACAO) {
        this.SITUACAO = SITUACAO;
    }

    /**
     * @return the DATA_PAGAMENTO
     */
    public String getDATA_PAGAMENTO() {
        return DATA_PAGAMENTO;
    }

    /**
     * @param DATA_PAGAMENTO the DATA_PAGAMENTO to set
     */
    public void setDATA_PAGAMENTO(String DATA_PAGAMENTO) {
        this.DATA_PAGAMENTO = DATA_PAGAMENTO;
    }
}
