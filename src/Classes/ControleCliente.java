package Classes;

import CONEXAO.ConexaoOracle;
import Validações.UltimaSequencia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControleCliente {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClasseCliente Cliente;

    public ControleCliente() {
        Cliente = new ClasseCliente();
        conecta_oracle = new ConexaoOracle();
    }

    public boolean Cadastrar(ClasseCliente obj) {
        try {
            UltimaSequencia us = new UltimaSequencia("ID_CLIENTE", "CLIENTE");
            obj.setID_CLIENTE(Integer.parseInt(us.getUtl()));
            String SQL = ("INSERT INTO CLIENTE(ID_CLIENTE, VALOR, NOME, TEL1, "
                    + "TEL2,DATA_ATUAL,DATA_VENCIMENTO,"
                    + "CIDADE,SITUACAO)"
                    + "VALUES ("
                    + obj.getID_CLIENTE() + ","
                    + obj.getVALOR() + ",'"
                    + obj.getNOME() + "','"
                    + obj.getTEL1() + "','"
                    + obj.getTEL2() + "','"
                    + obj.getDATA_ATUAL() + "','"
                    + obj.getDATA_VENCIMENTO() + "','"
                    + obj.getCIDADE() + "','"
                    + obj.getSITUACAO() + "')");
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            obj.setERRO(String.valueOf(ex));
            JOptionPane.showMessageDialog(null, obj.getERRO());
            return false;
        }
    }

    public boolean Liquidar(ClasseCliente obj) {
        try {
            pst = conn.prepareStatement(" UPDATE CLIENTE SET SITUACAO = ?,DATA_PAGAMENTO = ? "
                    + " WHERE ID_CLIENTE = ?");
            pst.setString(1, obj.getSITUACAO());
            pst.setString(2, obj.getDATA_PAGAMENTO());
            pst.setInt(3, obj.getID_CLIENTE());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public String DATA_ATUAL() {
        String SQL = "SELECT TO_CHAR (SYSDATE, 'DD/MM/YYYY') AS DATA FROM DUAL";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            SQL = conecta_oracle.resultSet.getString("DATA");
        } catch (Exception ex) {
            System.out.println(ex);
            SQL = null;
        }
        return SQL;
    }

    public ResultSet Pesquisar() {
        String SQL = "SELECT NOME, VALOR, TO_CHAR (DATA_VENCIMENTO,'DD/MM/YYYY') AS DATA_VENCIMENTO,"
                + "TO_CHAR (DATA_ATUAL, 'DD/MM/YYYY') AS DATA_ATUAL, TEL1, TEL2, ID_CLIENTE FROM CLIENTE"
                + " ORDER BY NOME ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public boolean Excluir(ClasseCliente obj) {
        String SQL = "DELETE FROM CLIENTE WHERE ID_CLIENTE = " + obj.getID_CLIENTE();
        try {
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public ResultSet Pesquisar(String Nome) {
        String SQL = "SELECT NOME, VALOR, TO_CHAR (DATA_VENCIMENTO,'DD/MM/YYYY') AS DATA_VENCIMENTO,"
                + "TO_CHAR (DATA_ATUAL, 'DD/MM/YYYY') AS DATA_ATUAL, TEL1, TEL2, ID_CLIENTE FROM CLIENTE"
                + " WHERE NOME LIKE '" + Nome + "' ORDER BY NOME ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public ResultSet PesquisarVencido() {
        String SQL = "SELECT NOME, VALOR, TO_CHAR (DATA_VENCIMENTO,'DD/MM/YYYY') AS DATA_VENCIMENTO,"
                + " TO_CHAR (DATA_ATUAL, 'DD/MM/YYYY') AS DATA_ATUAL, TEL1, TEL2,ID_CLIENTE FROM CLIENTE"
                + " WHERE SITUACAO = 'ABERTA'"
                + " ORDER BY DATA_VENCIMENTO ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public ResultSet PesquisarPagos() {
        String SQL = "SELECT NOME, VALOR, TO_CHAR (DATA_VENCIMENTO,'DD/MM/YYYY') AS DATA_VENCIMENTO,"
                + " TO_CHAR (DATA_ATUAL, 'DD/MM/YYYY') AS DATA_ATUAL, TEL1, TEL2,ID_CLIENTE FROM CLIENTE"
                + " WHERE SITUACAO = 'LIQUIDADA'"
                + " ORDER BY DATA_VENCIMENTO ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public ResultSet PesquisarPagosCad() {
        String SQL = "SELECT NOME, VALOR, TO_CHAR (DATA_PAGO,'DD/MM/YYYY') AS DATA, ID_PAGO FROM CAD_PAGO";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public ResultSet PesquisarPagosCad(String NOME) {
        String SQL = "SELECT NOME, VALOR, TO_CHAR (DATA_PAGO,'DD/MM/YYYY') AS DATA, ID_PAGO FROM CAD_PAGO"
                + " WHERE NOME LIKE '" + NOME + "' ";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public boolean CadastrarPagos(ClassePago obj) {
        try {
            UltimaSequencia us = new UltimaSequencia("ID_PAGO", "CAD_PAGO");
            obj.setID_PAGO(Integer.parseInt(us.getUtl()));
            String SQL = ("INSERT INTO CAD_PAGO(ID_PAGO, DATA_PAGO, NOME, VALOR)"
                    + "VALUES ("
                    + obj.getID_PAGO() + ",'"
                    + obj.getDATA_PAGO() + "','"
                    + obj.getNOME() + "',"
                    + obj.getVALOR() + ")");
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            obj.setERRO(String.valueOf(ex));
            JOptionPane.showMessageDialog(null, obj.getERRO());
            return false;
        }
    }

    public boolean ExcluirPagos(ClassePago obj) {
        try {
            String SQL = ("DELETE FROM CAD_PAGO"
                    + " WHERE ID_PAGO = "
                    + obj.getID_PAGO());
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            obj.setERRO(String.valueOf(ex));
            JOptionPane.showMessageDialog(null, obj.getERRO());
            return false;
        }
    }
    public ResultSet PesquisarCobranca(){
     String SQL = "SELECT NOME, VALOR, TO_CHAR(DATA_PAGO,'DD/MM/YYYY') AS DATA FROM CAD_PAGO ORDER BY NOME ASC";
     conecta_oracle.executeSQL(SQL);
     return rs = conecta_oracle.resultSet;
    }
   

}
