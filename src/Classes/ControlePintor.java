package Classes;

import CONEXAO.ConexaoOracle;
import Validações.UltimaSequencia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

public class ControlePintor {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClassePintor Classe_Pintor;

    public ControlePintor() {
        Classe_Pintor = new ClassePintor();
        conecta_oracle = new ConexaoOracle();
    }

    public ResultSet PesquisarGrupo(String Grupo) {

        String SQL = "SELECT NOME, TEL1,TEL2 FROM CAD_PINTOR WHERE GRUPO = '" + Grupo + "'";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public boolean AlterarSemData(ClassePintor obj) {
        try {
            pst = conn.prepareStatement("UPDATE CAD_PINTOR SET "
                    + "TEL1 = ?"
                    + ",TEL2 = ?,"
                    + "NOME = ?,"
                    + "ENDERECO=?"
                    + ",NUMERO = ?,"
                    + "CIDADE= ?,"
                    + "DATA_NASCIMENTO = ?,"
                    + "OBS = ?,"
                    + "GRUPO = ?,"
                    + "VENDEDOR = ?"
                    + " WHERE ID_PINTOR = ?");
            pst.setString(1, obj.getTEL1());
            pst.setString(2, obj.getTEL2());
            pst.setString(3, obj.getNOME());
            pst.setString(4, obj.getENDERECO());
            pst.setString(5, obj.getNUMERO());
            pst.setString(6, obj.getCIDADE());
            pst.setString(7, obj.getDATA_NASCIMENTO());
            pst.setString(8, obj.getOBS());
            pst.setString(9, obj.getGRUPO());
            pst.setString(10, obj.getVENDEDOR());
            pst.setInt(11, obj.getID_PINTOR());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean AlterarCom(ClassePintor obj) {
        try {
            pst = conn.prepareStatement("UPDATE CAD_PINTOR SET "
                    + "TEL1 = ?"
                    + ",TEL2 = ?,"
                    + "NOME = ?,"
                    + "ENDERECO=?"
                    + ",NUMERO = ?,"
                    + "CIDADE= ?,"
                    + "OBS = ?,"
                    + "DATA_NASCIMENTO = ?,"
                    + "GRUPO = ?, "
                    + "VENDEDOR = ?"
                    + "WHERE ID_PINTOR = ?");
            pst.setString(1, obj.getTEL1());
            pst.setString(2, obj.getTEL2());
            pst.setString(3, obj.getNOME());
            pst.setString(4, obj.getENDERECO());
            pst.setString(5, obj.getNUMERO());
            pst.setString(6, obj.getCIDADE());
            pst.setString(7, obj.getOBS());
            pst.setString(8, obj.getDATA_NASCIMENTO());
            pst.setString(9, obj.getGRUPO());
            pst.setString(10, obj.getVENDEDOR());
            pst.setInt(11, obj.getID_PINTOR());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean CadastrarSemData(ClassePintor obj) {
        try {
            UltimaSequencia us = new UltimaSequencia("ID_PINTOR", "CAD_PINTOR");
            obj.setID_PINTOR(Integer.parseInt(us.getUtl()));
            String SQL = ("INSERT INTO CAD_PINTOR (ID_PINTOR,TEL1,TEL2,NOME,ENDERECO,NUMERO,CIDADE,"
                    + "DATA_NASCIMENTO,OBS,GRUPO,VENDEDOR) VALUES ("
                    + obj.getID_PINTOR() + ",'"
                    + obj.getTEL1() + "','"
                    + obj.getTEL2() + "','"
                    + obj.getNOME() + "','"
                    + obj.getENDERECO() + "','"
                    + obj.getNUMERO() + "','"
                    + obj.getCIDADE() + "',"
                    + obj.getDATA_NASCIMENTO() + ",'"
                    + obj.getOBS() + "','"
                    + obj.getGRUPO() + "','"
                    + obj.getVENDEDOR() + "')");
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            obj.setERRO(String.valueOf(ex));
            JOptionPane.showMessageDialog(null, obj.getERRO());
            return false;
        }
    }

    public boolean CadastrarComData(ClassePintor obj) {
        try {
            UltimaSequencia us = new UltimaSequencia("ID_PINTOR", "CAD_PINTOR");
            obj.setID_PINTOR(Integer.parseInt(us.getUtl()));
            String SQL = ("INSERT INTO CAD_PINTOR (ID_PINTOR,TEL1,TEL2,NOME,ENDERECO,NUMERO,CIDADE,"
                    + "DATA_NASCIMENTO,OBS,GRUPO,VENDEDOR) VALUES ("
                    + obj.getID_PINTOR() + ",'"
                    + obj.getTEL1() + "','"
                    + obj.getTEL2() + "','"
                    + obj.getNOME() + "','"
                    + obj.getENDERECO() + "','"
                    + obj.getNUMERO() + "','"
                    + obj.getCIDADE() + "','"
                    + obj.getDATA_NASCIMENTO() + "','"
                    + obj.getOBS() + "','"
                    + obj.getGRUPO() + "','"
                    + obj.getVENDEDOR() + "')");
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            obj.setERRO(String.valueOf(ex));
            JOptionPane.showMessageDialog(null, obj.getERRO());
            return false;
        }
    }

    public boolean Excluir(ClassePintor obj) {
        String SQL = "DELETE FROM CAD_PINTOR WHERE ID_PINTOR = " + obj.getID_PINTOR();
        try {
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public ResultSet Pesquisar() {
        String SQL = "SELECT VENDEDOR,NOME,TEL1,TEL2,CIDADE,ENDERECO,NUMERO,"
                + "TO_CHAR(DATA_NASCIMENTO,'DD/MM/YYYY') AS DATA,OBS,GRUPO,ID_PINTOR "
                + " FROM CAD_PINTOR ORDER BY NOME ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public ResultSet Pesquisar(int Codigo) {
        String SQL = "SELECT VENDEDOR,NOME,TEL1,TEL2,CIDADE,ENDERECO,NUMERO,"
                + "TO_CHAR(DATA_NASCIMENTO,'DD/MM/YYYY') AS DATA,OBS,GRUPO,ID_PINTOR "
                + " FROM CAD_PINTOR WHERE ID_PINTOR = " + Codigo + " ORDER BY NOME ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public ResultSet Pesquisar(String Nome) {
        String SQL = "SELECT VENDEDOR,NOME,TEL1,TEL2,CIDADE,ENDERECO,NUMERO,"
                + "TO_CHAR(DATA_NASCIMENTO,'DD/MM/YYYY') AS DATA,OBS,GRUPO,ID_PINTOR "
                + " FROM CAD_PINTOR WHERE NOME LIKE '%" + Nome + "%' ORDER BY NOME ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public ResultSet PesquisarCidade(String Cidade) {
        String SQL = "SELECT VENDEDOR,NOME,TEL1,TEL2,CIDADE,ENDERECO,NUMERO,"
                + "TO_CHAR(DATA_NASCIMENTO,'DD/MM/YYYY') AS DATA,OBS,GRUPO,ID_PINTOR "
                + " FROM CAD_PINTOR WHERE CIDADE LIKE '%" + Cidade + "%' ORDER BY NOME ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public ResultSet PesquisarConsulta() {
        String SQL = "SELECT VENDEDOR,NOME, TEL1,TEL2,CIDADE,"
                + "TO_CHAR(DATA_NASCIMENTO,'DD/MM/YYYY') AS DATA FROM CAD_PINTOR ORDER BY NOME ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public ResultSet PesquisarConsulta(String Nome) {
        String SQL = "SELECT NOME, TEL1,TEL2,CIDADE,TO_CHAR(DATA_NASCIMENTO,'DD/MM/YYYY') AS DATA FROM CAD_PINTOR"
                + " WHERE NOME LIKE '%" + Nome + "%' ORDER BY NOME ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;

    }

    public String Data() {
        String Data;
        conecta_oracle.executeSQL("SELECT TO_CHAR(SYSDATE,'DD/MM') AS DATA FROM DUAL");
        try {
            conecta_oracle.resultSet.next();
            Data = conecta_oracle.resultSet.getString("DATA");

        } catch (Exception ex) {
            Data = null;
        }
        return Data;
    }

    public boolean Niver(String Data) {
        String SQL = "SELECT ID_PINTOR, NOME FROM CAD_PINTOR WHERE TO_CHAR(DATA_NASCIMENTO,'DD/MM') = '" + Data + "' ";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            int Codigo = conecta_oracle.resultSet.getInt("ID_PINTOR");
            String Nome = conecta_oracle.resultSet.getString("NOME");
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public ResultSet Aniversariantes(String Data) {
        String SQL = "SELECT NOME, TEL1,TEL2, CIDADE FROM CAD_PINTOR WHERE TO_CHAR(DATA_NASCIMENTO,'DD/MM') = '" + Data + "' ";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public ResultSet PesquisarGrupo1(String Grupo) {
        String SQL = "SELECT VENDEDOR,NOME,TEL1,TEL2 FROM CAD_PINTOR WHERE GRUPO = '" + Grupo + "'";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public void Imprimir() {
        conecta_oracle.conecta(0);
        String SQL = "SELECT * FROM CAD_PINTOR";
        try {
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(conecta_oracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Cobrança\\src\\"
                    + "ImprimirLista.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);
        } catch (Exception erro) {
            System.out.println(erro);
        }
    }
//******************** Aniversarios *********************//
    public ResultSet Janeiro() {
        String SQL = "SELECT VENDEDOR, NOME, TEL1, TEL2,CIDADE,"
                + " TO_CHAR(DATA_NASCIMENTO,'DD/MM')  FROM CAD_PINTOR "
                + " WHERE TO_CHAR(DATA_NASCIMENTO,'MM')"
                + " = '01'";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }
      public ResultSet Fevereiro() {
        String SQL = "SELECT VENDEDOR, NOME, TEL1, TEL2,CIDADE,"
                + " TO_CHAR(DATA_NASCIMENTO,'DD/MM')  FROM CAD_PINTOR "
                + " WHERE TO_CHAR(DATA_NASCIMENTO,'MM')"
                + " = '02' ";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }
      public ResultSet Marco() {
        String SQL = "SELECT VENDEDOR, NOME, TEL1, TEL2,CIDADE,"
                + " TO_CHAR(DATA_NASCIMENTO,'DD/MM')  FROM CAD_PINTOR "
                + " WHERE TO_CHAR(DATA_NASCIMENTO,'MM')"
                + " = '03'";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }
 public ResultSet Abril() {
        String SQL = "SELECT VENDEDOR, NOME, TEL1, TEL2,CIDADE,"
                + " TO_CHAR(DATA_NASCIMENTO,'DD/MM')  FROM CAD_PINTOR "
                + " WHERE TO_CHAR(DATA_NASCIMENTO,'MM')"
                + " = '04' ";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }
  public ResultSet Maio() {
        String SQL = "SELECT VENDEDOR, NOME, TEL1, TEL2,CIDADE,"
                + " TO_CHAR(DATA_NASCIMENTO,'DD/MM')  FROM CAD_PINTOR "
                + " WHERE TO_CHAR(DATA_NASCIMENTO,'MM')"
                + " = '05' ";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    } public ResultSet Junho() {
        String SQL = "SELECT VENDEDOR, NOME, TEL1, TEL2,CIDADE,"
                + " TO_CHAR(DATA_NASCIMENTO,'DD/MM')  FROM CAD_PINTOR "
                + " WHERE TO_CHAR(DATA_NASCIMENTO,'MM')"
                + " = '06'";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    } public ResultSet Julho() {
        String SQL = "SELECT VENDEDOR, NOME, TEL1, TEL2,CIDADE,"
                + " TO_CHAR(DATA_NASCIMENTO,'DD/MM')  FROM CAD_PINTOR "
                + " WHERE TO_CHAR(DATA_NASCIMENTO,'MM')"
                + " = '07'";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }
    
    public ResultSet Agosto() {
        String SQL = "SELECT VENDEDOR, NOME, TEL1, TEL2,CIDADE,"
                + " TO_CHAR(DATA_NASCIMENTO,'DD/MM')  FROM CAD_PINTOR "
                + " WHERE TO_CHAR(DATA_NASCIMENTO,'DMM')"
                + " = '08' ";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    } public ResultSet Setembro() {
        String SQL = "SELECT VENDEDOR, NOME, TEL1, TEL2,CIDADE,"
                + " TO_CHAR(DATA_NASCIMENTO,'DD/MM')  FROM CAD_PINTOR "
                + " WHERE TO_CHAR(DATA_NASCIMENTO,'MM')"
                + " = '09' ";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    } public ResultSet Outubro() {
        String SQL = "SELECT VENDEDOR, NOME, TEL1, TEL2,CIDADE,"
                + " TO_CHAR(DATA_NASCIMENTO,'DD/MM')  FROM CAD_PINTOR "
                + " WHERE TO_CHAR(DATA_NASCIMENTO,'MM')"
                + " = '10' ";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    } public ResultSet Novembro() {
        String SQL = "SELECT VENDEDOR, NOME, TEL1, TEL2,CIDADE,"
                + " TO_CHAR(DATA_NASCIMENTO,'DD/MM')  FROM CAD_PINTOR "
                + " WHERE TO_CHAR(DATA_NASCIMENTO,'MM')"
                + " = '11' ";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    } public ResultSet Dezembro() {
        String SQL = "SELECT VENDEDOR, NOME, TEL1, TEL2,CIDADE,"
                + " TO_CHAR(DATA_NASCIMENTO,'DD/MM')  FROM CAD_PINTOR "
                + " WHERE TO_CHAR(DATA_NASCIMENTO,'MM')"
                + " = '12' ";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }
}
