
package Classes;

public class ClassePago {
private int ID_PAGO;
private String DATA_PAGO;
private String NOME;
private Double VALOR;
private String ERRO;

    /**
     * @return the ID_PAGO
     */
    public int getID_PAGO() {
        return ID_PAGO;
    }

    /**
     * @param ID_PAGO the ID_PAGO to set
     */
    public void setID_PAGO(int ID_PAGO) {
        this.ID_PAGO = ID_PAGO;
    }

    /**
     * @return the DATA_PAGO
     */
    public String getDATA_PAGO() {
        return DATA_PAGO;
    }

    /**
     * @param DATA_PAGO the DATA_PAGO to set
     */
    public void setDATA_PAGO(String DATA_PAGO) {
        this.DATA_PAGO = DATA_PAGO;
    }

    /**
     * @return the NOME
     */
    public String getNOME() {
        return NOME;
    }

    /**
     * @param NOME the NOME to set
     */
    public void setNOME(String NOME) {
        this.NOME = NOME;
    }

    /**
     * @return the VALOR
     */
    public Double getVALOR() {
        return VALOR;
    }

    /**
     * @param VALOR the VALOR to set
     */
    public void setVALOR(Double VALOR) {
        this.VALOR = VALOR;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }

}
