package Classes;

import CONEXAO.ConexaoOracle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControleUsuario {

    ResultSet Rs;
    ConexaoOracle conecta_Oracle;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    private ClasseUsuario Classe_User;

    public ControleUsuario() {
        conecta_Oracle = new ConexaoOracle();
        conecta_Oracle.conecta(0);
        Classe_User = new ClasseUsuario();
    }

    public boolean RetornaUsuario(ClasseUsuario obj) {
        String SQL = "SELECT * FROM USUARIO WHERE LOGIN = '" + obj.getUSUARIO() + "' AND SENHA = '" + obj.getSENHA() + "'";
        conecta_Oracle.executeSQL(SQL);
        try {
            conecta_Oracle.resultSet.first();
            String Senha = conecta_Oracle.resultSet.getString("SENHA");
            String Usuario = conecta_Oracle.resultSet.getString("LOGIN");
            return true;
        } catch (SQLException ex) {
            obj.setERRO("Usuário e/ou Senha Incorretos");
            return false;
        }
    }

    public boolean Cadastrar(ClasseUsuario obj) {
        try {
            pst = conn.prepareStatement("INSERT INTO USUARIO (LOGIN,SENHA) "
                    + "VALUES ('" + obj.getUSUARIO() + "','"
                    + obj.getSENHA() + "')");
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean BuscarUsuario(ClasseUsuario obj) {
        String SQL = "SELECT * FROM USUARIO WHERE LOGIN = '" + obj.getUSUARIO() + "'";
        conecta_Oracle.executeSQL(SQL);
        try {
            if (ConexaoOracle.resultSet.next()) {
                return true;
            } else {
                obj.setERRO("Usuário já Cadastrado");
                return false;
            }
        } catch (SQLException ex) {
            return false;
        }
    }
}
