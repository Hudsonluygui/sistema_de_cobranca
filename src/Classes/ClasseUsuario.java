
package Classes;


public class ClasseUsuario {
      private String USUARIO;
    private String SENHA;
    private String ERRO;

    /**
     * @return the USUARIO
     */
    public String getUSUARIO() {
        return USUARIO;
    }

    /**
     * @param USUARIO the USUARIO to set
     */
    public void setUSUARIO(String USUARIO) {
        this.USUARIO = USUARIO;
    }

    /**
     * @return the SENHA
     */
    public String getSENHA() {
        return SENHA;
    }

    /**
     * @param SENHA the SENHA to set
     */
    public void setSENHA(String SENHA) {
        this.SENHA = SENHA;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }
}
