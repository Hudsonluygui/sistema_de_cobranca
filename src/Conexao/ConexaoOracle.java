package CONEXAO;

import java.sql.Statement;
import java.sql.*;
import javax.swing.*;

public class ConexaoOracle {

    public static Connection ConexaoOracle;  //OBJETO DE CONEXAO COM O BANCO
    public static Statement statement;       //ENVIA
    public static ResultSet resultSet;       //RETORNO DO BANCO DE DADOS
    public ResultSetMetaData metaData;       //RETORNO DO TIPO

    public static Connection conecta(int banco) {

        if (ConexaoOracle != null) {
            return ConexaoOracle;
        } else {
            try {
                Class.forName("oracle.jdbc.driver.OracleDriver");
                ConexaoOracle = DriverManager.getConnection(
                        "jdbc:oracle:thin:@127.0.0.1:1521"
                        + ":XE", "COBRANCA", "COBRANCA");
                System.out.println("Conectado");
                //JOptionPane.showMessageDialog(null, "CONECTADO");
                return ConexaoOracle;

            } catch (ClassNotFoundException ex) {
                JOptionPane.showMessageDialog(null, "Driver Não " + "Localizado");
                ex.printStackTrace();
                return null;

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Erro na " + "Conexao com a fonte de Dados: Verifique se o Banco esta Iniciado");
                ex.printStackTrace();
                return null;
            }
        }
    }

    public void desconecta() {
        boolean result = true;
        try {
            ConexaoOracle.close();
            System.out.println("Desconectado");
        } catch (SQLException fecha) {
            JOptionPane.showMessageDialog(null, "Não foi possível" + " Fechar o Banco de Dados: " + fecha);
            result = false;
        }
    }

    public void executeSQL(String sql) {
        try {
            statement = ConexaoOracle.createStatement(
                    ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.execute("ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY'");
            resultSet = statement.executeQuery(sql);

        } catch (SQLException sqlex) {
            String strl = (sqlex.toString());
            String procurada = "ORA-0001";
            int X = strl.indexOf(procurada);
            if (X != -1) {

                JOptionPane.showMessageDialog(null, "Registro ja cadastrado \n" + sqlex);
            } else {

                JOptionPane.showMessageDialog(null, "Não foi possivel " + "executar o comando SQL, " + sqlex + ", o SQL passado foi " + sql);
            }
        }
        try {
            metaData = resultSet.getMetaData();
        } catch (SQLException erro) {
            return;
            //  JOptionPane.showMessageDialog(null, erro);
        }

    }

}
